update slot_master set normal_tariff = case 
	WHEN id IN (1,2,3,4) THEN 2000
	WHEN id IN (5,6,7,8,9,10,11,12) THEN 2600
	WHEN id IN (13,14,15) THEN 2000
	WHEN id IN (16,17,18,19,20,21,22,23,24) THEN 2600 
	ELSE normal_tariff END,
	weekend_tariff = case
	WHEN id IN (1,2,3,4) THEN 2600
	WHEN id IN (5,6,7,8,9,10,11,12) THEN 2900
	WHEN id IN (13,14,15) THEN 2600
	WHEN id IN (16,17,18,19,20,21,22,23,24) THEN 2900 
	ELSE weekend_tariff END
	
	