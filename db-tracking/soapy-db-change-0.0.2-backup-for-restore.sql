update slot_master set normal_tariff = case 
	WHEN id IN (1,2,3,4,5) THEN 2500
	WHEN id IN (6,7,8,9,10,11,12) THEN 2800
	WHEN id IN (13,14,15,16) THEN 2500
	WHEN id IN (17,18,19,20,21,22,23,24) THEN 2800 
	ELSE normal_tariff END,
	weekend_tariff = case
	WHEN id IN (1,2,3,4,5) THEN 2800
	WHEN id IN (6,7,8,9,10,11,12) THEN 3000
	WHEN id IN (13,14,15,16) THEN 2800
	WHEN id IN (17,18,19,20,21,22,23,24) THEN 3000 
	ELSE weekend_tariff END
	
	