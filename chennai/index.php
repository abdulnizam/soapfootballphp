<?php
ini_set('always_populate_raw_post_data', '-1');

// phpinfo();
// die;
$down='false'; // 'true' or 'false'
if($down == 'true') {
  include '../chennai-maintence.php';
  die;
}

if ($_SERVER['REQUEST_URI'] === '/') {
    // Redirect to "/admin"
    header('Location: /admin');
    exit(); // Ensure that script execution stops after the redirect
}
// Define path to application directory
defined('APPLICATION_PATH')
    || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../app'));

defined('CONFIG_PATH')
    || define('CONFIG_PATH', realpath(dirname(__FILE__) . '/configs'));

    // Define application environment
defined('APPLICATION_ENV')
    || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'DEV'));


defined('UPLOAD_PATH') || define('UPLOAD_PATH', realpath(dirname(__FILE__).'/uploads'));
    
// Ensure library/ is on include_path
set_include_path(implode(PATH_SEPARATOR, array(
    realpath(APPLICATION_PATH . '/../library'),
    get_include_path(),
)));
date_default_timezone_set('Asia/Kolkata');

/** Zend_Application */
require_once 'Zend/Application.php';

// Create application, bootstrap, and run
$application = new Zend_Application(
    APPLICATION_ENV,
    APPLICATION_PATH . '/configs/application.ini'
);
$application->bootstrap()
            ->run();
