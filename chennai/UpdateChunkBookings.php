<?php 

// Define path to application directory
defined('APPLICATION_PATH')
|| define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../app'));

defined('CONFIG_PATH')
|| define('CONFIG_PATH', realpath(dirname(__FILE__) . '/configs'));
// Define application environment
defined('APPLICATION_ENV')
|| define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'PROD'));

// Ensure library/ is on include_path
set_include_path(implode(PATH_SEPARATOR, array(
		realpath(APPLICATION_PATH . '/../library'),
		get_include_path(),
)));

date_default_timezone_set('Asia/Kolkata');

require_once 'Zend/Config/Ini.php';
require_once 'Zend/Db/Adapter/Pdo/Mysql.php';
require_once 'Zend/Registry.php';
require_once 'Zend/Mail/Transport/Smtp.php';
require_once 'Zend/Mail.php';
require_once 'Zend/Db/Table/Abstract.php';
require_once 'SOAPY/Model/Bookings.php';

$config = (new Zend_Config_Ini(CONFIG_PATH."/config.ini", APPLICATION_ENV))->toArray();
$db = new Zend_Db_Adapter_Pdo_Mysql($config['db']);
$registry = Zend_Registry::getInstance();
$registry->set("defaultDB", $db);
echo "Chunk Order Update Started.";
$bookingsModel = SOAPY_Model_Bookings::getInstance(); 
$bookingsModel->updateChunckOrders();
echo "Chunk Order Update completed";

?>
