<?php
class SOAPY_Helper_MailHelper{
	
	public static function sendConfirmationMail($to, $data){
		if(!empty($to)){
			$mailConfig = Zend_Registry::get('mail_config');
			$subject = $mailConfig['mail']['confirmationMailSubject'];
			$message = $mailConfig['mail']['confirmationMailTemplate'];
			$message = str_replace('{name}',$data['name'], $message);
			$message = str_replace('{order_id}',$data['order_id'], $message);
			$message = str_replace('{status}',$data['status'], $message);
			$message = str_replace('{mobile}',$data['mobile'], $message);
			$slotsArr = array();
			foreach($data['slots'] as $slot){
				array_push($slotsArr, "Date : ".$slot['booking_date']." Court : ". $slot['court_name'] ." Slot : ".$slot['slot']);
			}
			
			$message = str_replace('{slots}',implode('<br/>', $slotsArr), $message);
			self::sendMail($to, $subject, $message);
		}
	}
	
		
	public static function sendMail($to, $subject, $message){
		/* $mailLogModel = STRGYM_Model_EmailLog::getInstance();
		$data = array();
		$data['to_address'] = $to;
		$data['subject'] = $subject;
		$data['content'] = $message;
		$mailLogModel->save($data); */
		$mailConfig = Zend_Registry::get('mail_config');
		$mail = new Zend_Mail();
		$mail->setFrom($mailConfig["mail"]["from"], $mailConfig["mail"]["fromName"]);
		$mail->addTo($to);
		$mail->addBcc($mailConfig["mail"]["from"]);
		$mail->setSubject($subject);
		$mail->setBodyHtml($message);
		$mail->send();
	}
}
