<?php
class SOAPY_Helper_Payment_PayUMoney {
	
	public static function getRequestHash($data){
		$config = Zend_Registry::get("config");
		$SALT = $config['payment']['payumoney']['salt'];
		
		$hash = '';
		// Hash Sequence
		$hashSequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";
		$hashVarsSeq = explode('|', $hashSequence);
		$hash_string = '';
		foreach($hashVarsSeq as $hash_var) {
			$hash_string .= isset($data[$hash_var]) ? $data[$hash_var] : '';
			$hash_string .= '|';
		}
	
		$hash_string .= $SALT;
	
	
		$hash = strtolower(hash('sha512', $hash_string));
		return $hash;
		
	}
	public static function getResponseHash($data){
		$status=$data["status"];
		$firstname=$data["firstname"];
		$amount=$data["amount"];
		$txnid=$data["txnid"];
		
		$posted_hash=$data["hash"];
		$key=$data["key"];
		$productinfo=$data["productinfo"];
		$email=$data["email"];
		$salt="RdLCnJzxQx";
		
		if (isset($data["additionalCharges"])) {
			$additionalCharges=$data["additionalCharges"];
			$retHashSeq = $additionalCharges.'|'.$salt.'|'.$status.'|||||||||||'.$email.'|'.$firstname.'|'.$productinfo.'|'.$amount.'|'.$txnid.'|'.$key;
		
		}
		else {
		
			$retHashSeq = $salt.'|'.$status.'|||||||||||'.$email.'|'.$firstname.'|'.$productinfo.'|'.$amount.'|'.$txnid.'|'.$key;
		
		}
		$hash = hash("sha512", $retHashSeq);
		
		return $hash;
		
		
	}
	public static function getGatewayUrl($env = "test"){
		$config = Zend_Registry::get("config");
		if($env == "prod"){
			return $config["payment"]['payumoney']["prod"]['url'];
		}
		else{
			return $config["payment"]['payumoney']["test"]['url'];
		}
	}
	
	
}

