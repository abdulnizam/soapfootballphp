<?php
class SOAPY_Helper_Payment_CCAvenue {
	
	public static function getEncryptedData($data, $env = "test"){
		$merchant_data='';
		$working_key= self::getWorkingKey($env);
		$access_code= self::getAccessCode($env);
		
		foreach ($data as $key => $value){
			$merchant_data.=$key.'='.$value.'&';
		}
		
		$encrypted_data= self::encrypt($merchant_data,$working_key); // Method for encrypting the data.
		
		return $encrypted_data;
	}
	
	public static function getOrderStatusWithInformation($receivedData, $env = "test"){
		$workingKey= self::getWorkingKey($env);		//Working Key should be provided here.
		$encResponse= $receivedData;			//This is the response sent by the CCAvenue Server
		$rcvdString=self::decrypt($encResponse,$workingKey);		//Crypto Decryption used as per the specified working key.
		$order_status="";
		$decryptValues=explode('&', $rcvdString);
		$dataSize=sizeof($decryptValues);
		$orderInfo = array();
		$ret = array("status"=>"success");
		for($i = 0; $i < $dataSize; $i++) 
		{
			$information=explode('=',$decryptValues[$i]);
			$orderInfo[$information[0]] = $information[1];
			if($i==3)	$order_status=$information[1];
		}
		
	if($order_status==="Success")
	{
		$ret = array("status"=>"success", "order_status"=>"Success", "message"=>"Thank you for shopping with us. Your credit card has been charged and your transaction is successful. We will be shipping your order to you soon.", "orderInfo"=>$orderInfo);
		
	}
	else if($order_status==="Aborted")
	{
		$ret = array("status"=>"error", "order_status"=>"Aborted", "message"=>"Thank you for shopping with us.However,the transaction has been cancelled.", 'orderInfo' => $orderInfo);
	
	}
	else if($order_status==="Failure")
	{
		$ret = array("status"=>"error", "order_status"=>"Failiure", "message"=>"Thank you for shopping with us.However,the transaction has been declined.","orderInfo" => $orderInfo);
	}
	else
	{
		$ret = array("status"=>"error", "order_status"=>"Security Error", "message"=>"Security Error. Illegal access detected", "orderInfo"=>$orderInfo);
	
	}
		
		return $ret;
	}
	
	public static function getGatewayUrl($env = "test"){
		$config = Zend_Registry::get("config");
		if($env == "prod"){
			return $config["payment"]["prod"]['url'];
		}
		else{
			return $config["payment"]["test"]['url'];
		}
	}
	
	private static function getWorkingKey($env = "test"){
		$config = Zend_Registry::get("config");
		if($env == "prod"){
			return $config["payment"]["prod"]['working_key'];
		}
		else{
			return $config["payment"]["test"]['working_key'];
		}
	}
	
	public static function getAccessCode($env = "test"){
		$config = Zend_Registry::get("config");
		if($env == "prod"){
			return $config["payment"]["prod"]['access_code'];
		}
		else{
			return $config["payment"]["test"]['access_code'];
		}
	}
	
	
	public static function encrypt($plainText,$key)
	{
		$secretKey = self::hextobin(md5($key));
		$initVector = pack("C*", 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f);
		$openMode = mcrypt_module_open(MCRYPT_RIJNDAEL_128, '','cbc', '');
		$blockSize = mcrypt_get_block_size(MCRYPT_RIJNDAEL_128, 'cbc');
		$plainPad = self::pkcs5_pad($plainText, $blockSize);
		if (mcrypt_generic_init($openMode, $secretKey, $initVector) != -1)
		{
			$encryptedText = mcrypt_generic($openMode, $plainPad);
			mcrypt_generic_deinit($openMode);
			 
		}
		return bin2hex($encryptedText);
	}
	
	public static function decrypt($encryptedText,$key)
	{
		$secretKey = self::hextobin(md5($key));
		$initVector = pack("C*", 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f);
		$encryptedText=self::hextobin($encryptedText);
		$openMode = mcrypt_module_open(MCRYPT_RIJNDAEL_128, '','cbc', '');
		mcrypt_generic_init($openMode, $secretKey, $initVector);
		$decryptedText = mdecrypt_generic($openMode, $encryptedText);
		$decryptedText = rtrim($decryptedText, "\0");
		mcrypt_generic_deinit($openMode);
		return $decryptedText;
	
	}
	//*********** Padding Function *********************
	
	public static function pkcs5_pad ($plainText, $blockSize)
	{
		$pad = $blockSize - (strlen($plainText) % $blockSize);
		return $plainText . str_repeat(chr($pad), $pad);
	}
	
	//********** Hexadecimal to Binary function for php 4.0 version ********
	
	public static function hextobin($hexString)
	{
		$length = strlen($hexString);
		$binString="";
		$count=0;
		while($count<$length)
		{
			$subString =substr($hexString,$count,2);
			$packedString = pack("H*",$subString);
			if ($count==0)
			{
				$binString=$packedString;
			}
			 
			else
			{
				$binString.=$packedString;
			}
			 
			$count+=2;
		}
		return $binString;
	}
}

