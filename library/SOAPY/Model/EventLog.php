<?php
class SOAPY_Model_EventLog extends Zend_Db_Table_Abstract{
    
	/**
	 *
	 * @var SOAPY_Model_EventLog
	 */
    private static $_instance = null;
    
    /**
     * Singleton instance
     *
     * @return SOAPY_Model_EventLog
     */
    public static function getInstance(){
        if(null === self::$_instance){
        	self::$_instance = new self();
        }
        return self::$_instance;
    }
    
    public function __construct(){
    	$this->_db = Zend_Registry::get("defaultDB");
    	$this->_name = "event_log"; 
    }
    
    public function eventLog( $data )
    {
        $array['reference'] = $data['reference'];
        $array['payment_load'] = Zend_Json::encode($data['payment_load']);
        $array['message'] = $data['message'];
        $array['status'] = $data['status'];
        $array['date'] = date('Y-m-d h:i:sa');
        $event = $this->insert($array);
        return $event;
    }    
}
