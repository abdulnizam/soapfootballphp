<?php
class SOAPY_Model_Bookings extends Zend_Db_Table_Abstract{
    
	/**
	 *
	 * @var SOAPY_Model_Bookings
	 */
    private static $_instance = null;
    
    /**
     * Singleton instance
     *
     * @return SOAPY_Model_Bookings
     */
    public static function getInstance(){
        if(null === self::$_instance){
        	self::$_instance = new self();
        }
        return self::$_instance;
    }
    
    public function __construct(){
    	$this->_db = Zend_Registry::get("defaultDB");
    	$this->_name = "bookings"; 
    }
    
	public function getAllBookings($fromDate, $toDate = '', $status = array()){
    	
    	$select = $this->_db->select()->from($this->_name)
    				->joinLeft("slot_master", $this->_name.".slot_id = slot_master.id",array("slot"))
    				->joinLeft("courts", "slot_master.court_id = courts.id",array("court_name"));
    	if(empty($toDate)){
    		$select->where('booking_date = ?', $fromDate);
    	}
    	else{
    		$select->where("booking_date >= ?", $fromDate)->where("booking_date <= ?", $toDate);
    	}
    	if(!empty($status)){
    		$select->where("status in (?)",$status);
    	}
    	return $select->query()->fetchAll();
    }
    
    public function getAllBookingsByOrderId($orderId, $liveOrders =  false){
    	
    	$select = $this->_db->select()->from($this->_name)
    	->joinLeft("slot_master", $this->_name.".slot_id = slot_master.id",array("slot"))
    	->joinLeft("courts", "slot_master.court_id = courts.id",array("court_name"));
    	$select->where('order_id = ?', $orderId);
    	if($liveOrders){
    		$select->where("status = ?", 1);
    	}
    	return $select->query()->fetchAll();
    }
    
    public function getAllLiveBookings(){
    	$select = $this->_db->select()->from($this->_name)->joinLeft("slot_master", $this->_name.".slot_id = slot_master.id",array("slot"));
    	$select->where('booking_date >= ?', date('Y-m-d'));
    	return $select->query()->fetchAll();
    }
    
    public function save($data, $cartDetails){
    	$orderId = $this->getOrderId();
    	
    	foreach($cartDetails as $item){
    		$data['slot_id'] = $item['slot_id'];
    		$data['booking_date'] = $item['date'];
    		$data['is_booked_combo_offer'] = $item['is_booked_combo_offer'];
    		if(!empty($item['combo_offer'])){
    			$data['combo_offer'] = $item['combo_offer'];
    		}
    		$dbData = $this->sanitizeParamsForInsertOrUpdate($data);
    		$dbData['order_id'] = $orderId;
    		$dbData['booked_on']  = date('Y-m-d H:i:s');
    		$dbData['payment_started_on'] = date('Y-m-d H:i:s');
    		if(isset($data['booked_by']) && $data["booked_by"] == 1){ //booked by admin
    			$dbData['status'] = 1;
    			$dbData['payment_completed_on'] = date('Y-m-d H:i:s');
    		}
    		$bookingId = $this->insert($dbData);
    		 
    	}
    	
    	return $orderId;
    }
    
    public function sanitizeParamsForInsertOrUpdate($data){
    	$dbColumns = array('first_name', 'last_name', 'email_id', 'mobile_no', 'advance_amount','booking_date','slot_id','booked_by','is_booked_combo_offer','combo_offer');
    	$dbData = array();
    	foreach($data as $key => $value){
    		if(in_array($key, $dbColumns) && !empty($value)){
    			$dbData[$key] = $value;
    		}
    	}
    	return $dbData;
    }
    public function setOrderStatusSuccess($orderId, $paymentComments){
    	$this->update(array("payment_gateway_comments"=>$paymentComments, "status" => 1, "payment_completed_on"=>date("Y-m-d H:i:s")), array("order_id = ?"=>$orderId));
    	return true;
    }
    public function setOrderStatusPaymentFailiure($orderId, $paymentComments){
    	$this->update(array("payment_gateway_comments"=>$paymentComments, "status" => 2, "payment_completed_on"=>date("Y-m-d H:i:s")), array("order_id = ?"=>$orderId));
    	return true;
    }
    private function getOrderId(){
    	$result = $this->_db->select()->from("misc",array("last_order_id"))->query()->fetchAll();
    	$lastOrderId = $result[0]['last_order_id'];
    	$this->_db->update("misc",array("last_order_id"=>new Zend_Db_Expr("last_order_id+1")));
    	$config = Zend_Registry::get("config");
    	return $config['order_prefix'].$lastOrderId;
    }
    
    public function updateChunckOrders(){
    	$noOfRecords = $this->update(array("status"=>3, "payment_gateway_comments"=>"User has taken long time for payment."), array("TIMEDIFF(?, payment_started_on) > TIME('00:15:00')"=>date('Y-m-d H:i:s'),"status = ?" => 0 ));
    	echo $noOfRecords. " has been updated";
    	
    }
    
    public function cancelBooking($data){
    	$this->update(array("cancel_comments"=>$data['cancel_comments'],"cancelled_on"=>date('Y-m-d H:i:s'), "status"=>10), array("id = ?"=>$data['booking_id']));
    	return $data['booking_id'];
    }
   
    
    
}