<?php
class SOAPY_Model_Feedback extends Zend_Db_Table_Abstract{
    
	/**
	 *
	 * @var SOAPY_Model_Feedback
	 */
    private static $_instance = null;
    
    /**
     * Singleton instance
     *
     * @return SOAPY_Model_Feedback
     */
    public static function getInstance(){
        if(null === self::$_instance){
        	self::$_instance = new self();
        }
        return self::$_instance;
    }
    
    public function __construct(){
    	$this->_db = Zend_Registry::get("defaultDB");
    	$this->_name = "feedback"; 
    }
    
    public function getAllFeedbacks($fromData, $toDate){
    	return $this->fetchAll(array("is_active"=>1))->toArray();
    }
    
    public function entry($data){
    	$data['feedback_date'] = date('Y-m-d');
    	$data['feedback'] = Zend_Json::encode($data['feedback']);
    	$feedbackId = $this->insert($data);
    	return $feedbackId;
    }
    
}