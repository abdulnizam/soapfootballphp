<?php
class SOAPY_Model_SlotMaster extends Zend_Db_Table_Abstract{
    
	/**
	 *
	 * @var SOAPY_Model_SlotMaster
	 */
    private static $_instance = null;
    
    /**
     * Singleton instance
     *
     * @return SOAPY_Model_SlotMaster
     */
    public static function getInstance(){
        if(null === self::$_instance){
        	self::$_instance = new self();
        }
        return self::$_instance;
    }
    
    public function __construct(){
    	$this->_db = Zend_Registry::get("defaultDB");
    	$this->_name = "slot_master"; 
    }
    
    public function getAllSlots(){
    	 
    	return $this->fetchAll(array("is_active"=>1))->toArray();
    	
    	 
    }
	public function getAllAvailableSlots($date){
    	
    	$select = $this->_db->select()->from($this->_name,array("id","slot","normal_tariff","weekend_tariff","court_id",'available_days',"(bookings.id IS NOT NULL) AS is_booked","(slot_maintenance.id IS NOT NULL) is_maintenance","related_slots",'combo_offer_slots','combo_offer'))
    									->joinLeft("bookings", "slot_master.id = bookings.slot_id and booking_date = '".$date."' and bookings.status in (0,1)", array('status', 'booking_id'=>'id'))
    									->joinLeft("courts", "slot_master.court_id = courts.id", array("court_name","short_code"))
    									->joinLeft("slot_maintenance","slot_maintenance.slot_id = slot_master.id and maintenance_date = '".$date."' and slot_maintenance.status = 1",array())
    									->where('date(\''.$date.'\') >= date(?)', date('Y-m-d'))
    									->where ('( expire_date is null or date(?) <= expire_date)', $date)
    									->where ('( start_date is null or date(?) >= start_date)', $date)
    									->order(array("slot_master.court_id asc", "slot_master.order_index asc"));
    	
    	if(strtotime($date) ==  strtotime(date('Y-m-d'))){
    		$select->where('(slot_master.cutoff_time IS NULL or slot_master.cutoff_time > TIME(?))',date('H:i:s'));
    	}
    	
    	$slotMasterData = $select->query()->fetchAll();
    	return $this->transformCourtwiseSlot($slotMasterData, $date);
    	
    }
    public function isPublicHoliday($date){
    	return $this->_db->select()->from("public_holidays")->where("public_holiday = ?", $date)->query()->fetchAll();
    }
    
    public function getAllSlotsWithDetails($date){
    	 
    	$select = $this->_db->select()->from($this->_name,array("id","slot","normal_tariff","weekend_tariff","court_id",'available_days',"(bookings.id IS NOT NULL) AS is_booked","(slot_maintenance.id IS NOT NULL) is_maintenance", "related_slots"))
			    	->joinLeft("bookings", "slot_master.id = bookings.slot_id and booking_date = '".$date."' and bookings.status=1", array('booking_id'=>'id','order_id','first_name','last_name','mobile_no','email_id','booking_date','payment_gateway_comments','booked_by','booked_on',"status","is_booked_combo_offer","combo_offer"))
			    	->joinLeft("courts", "slot_master.court_id = courts.id", array("court_name","short_code"))
			    	->joinLeft("slot_maintenance","slot_maintenance.slot_id = slot_master.id and maintenance_date = '".$date."' and slot_maintenance.status = 1",array('maintenance_id'=>'id','maintenance_comments'))
			    	->where ('( expire_date is null or date(?) <= expire_date)', $date)
			    	->where ('( start_date is null or date(?) >= start_date)', $date)
			    	->order(array("slot_master.court_id asc", "slot_master.order_index asc"));
    	
    	$slotMasterData = $select->query()->fetchAll();
    	return $this->transformCourtwiseSlot($slotMasterData, $date);
    }
    
    
    
    private function transformCourtwiseSlot($slotMasterData, $date){
    	$ret = array();
	$isPublicHoliday = $this->isPublicHoliday($date);
	$relatedBookedSlots = Array();
    	foreach($slotMasterData as $slotMaster){
    		if(empty($slotMaster['available_days']) || (strpos($slotMaster['available_days'], date('w',strtotime($date))) !== false) || (!empty($isPublicHoliday) && (strpos($slotMaster['available_days'],'7') !== false))){
	    		if(empty($ret[$slotMaster['court_id']])){
	    			$ret[$slotMaster['court_id']] = array('court_id'=>$slotMaster['court_id'],'court_name'=>$slotMaster['court_name']);
	    			$ret[$slotMaster['court_id']]['slots']= array();
	    		}
			if(!empty($isPublicHoliday)){
				$slotMaster['isPublicHoliday'] = true;
			}
			// if($slotMaster['status'] == null) {
			// 	$slotMaster['status'] = "0";
			// }
			if($slotMaster['status'] === '0' || $slotMaster['status'] === '1' || $slotMaster['is_maintenance'] === '1'){
				$relatedSlots = explode(',',$slotMaster['related_slots']);
				foreach($relatedSlots as $relatedSlot){
					if(!in_array($relatedSlot, $relatedBookedSlots)){
						array_push($relatedBookedSlots, $relatedSlot);
					}
				}
			}
	    		array_push($ret[$slotMaster['court_id']]['slots'], $slotMaster);
    		}
    	}
    	
    	foreach($ret as $courtId => $slots){
    		foreach($slots['slots'] as $index => $slot){
    			if(in_array($slot['slot'], $relatedBookedSlots)){
    				$ret[$courtId]['slots'][$index]['status'] = -1;
    				$ret[$courtId]['slots'][$index]['is_booked'] = 1;
    			}
    		}
    	}
    	return $ret;
    }
    
   
    
    
}
