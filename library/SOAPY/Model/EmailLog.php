<?php
class STRGYM_Model_EmailLog extends Zend_Db_Table_Abstract{
    
	/**
	 *
	 * @var STRGYM_Model_EmailLog
	 */
    private static $_instance = null;
    
    /**
     * Singleton instance
     *
     * @return STRGYM_Model_EmailLog
     */
    public static function getInstance(){
        if(null === self::$_instance){
        	self::$_instance = new self();
        }
        return self::$_instance;
    }
    
    public function __construct(){
    	$this->_db = Zend_Registry::get("defaultDB");
    	$this->_name = "email_log"; 
    }
    
    public function getUnsentEmails($date){
    	return $this->fetchAll(array("is_sent = ?"=>0))->toArray();
    }
    
    public function save($data){
    	$dbData = $this->sanitizeParamsForInsertOrUpdate($data);
    	$this->insert($dbData);
    	
    	
    }
    public function sanitizeParamsForInsertOrUpdate($data){
    	$dbColumns = array('to_address', 'subject', 'content');
    	$dbData = array();
    	foreach($data as $key => $value){
    		if(in_array($key, $dbColumns) && !empty($value)){
    			$dbData[$key] = $value;
    		}
    	}
    	return $dbData;
    }
 	public function updateAllAsSent(){
    	$this->update(array("is_sent"=>1));
    }
   
    
    
}