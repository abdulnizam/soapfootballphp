<?php
class SOAPY_Model_Payments extends Zend_Db_Table_Abstract{
    
	/**
	 *
	 * @var SOAPY_Model_Payments
	 */
    private static $_instance = null;
    
    /**
     * Singleton instance
     *
     * @return SOAPY_Model_Payments
     */
    public static function getInstance(){
        if(null === self::$_instance){
        	self::$_instance = new self();
        }
        return self::$_instance;
    }
    
    public function __construct(){
    	$this->_db = Zend_Registry::get("defaultDB");
    	$this->_name = "payments"; 
    }
    
    
    
	public function entry($data){
		$data['payment_info'] = json_encode($data['payment_info']);
    	return $this->insert($data);
    }
    
    
}