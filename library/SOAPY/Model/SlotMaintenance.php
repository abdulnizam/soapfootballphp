<?php
class SOAPY_Model_SlotMaintenance extends Zend_Db_Table_Abstract{
    
	/**
	 *
	 * @var SOAPY_Model_SlotMaintenance
	 */
    private static $_instance = null;
    
    /**
     * Singleton instance
     *
     * @return SOAPY_Model_SlotMaintenance
     */
    public static function getInstance(){
        if(null === self::$_instance){
        	self::$_instance = new self();
        }
        return self::$_instance;
    }
    
    public function __construct(){
    	$this->_db = Zend_Registry::get("defaultDB");
    	$this->_name = "slot_maintenance"; 
    }
    
    public function getLiveSlotMaintenanceList(){
    	return $this->_db->select()->from($this->_name)->joinLeft("slot_master", $this->_name.".slot_id = slot_master.id",array("slot"))->where("maintenance_date >= ?", date('Y-m-d'))->query()->fetchAll();
    }
    public function getMaintenanceList($date){
    	return $this->_db->select()->from($this->_name)->joinLeft("slot_master", $this->_name.".slot_id = slot_master.id",array("slot"))->where("maintenance_date = ?", $date)->query()->fetchAll();
    }
    public function save($data){
    	$dbData = $this->sanitizeParamsForInsertOrUpdate($data);
    	$dbData['blocked_on'] = new Zend_Db_Expr('Now()');
    	$dbData['status'] = 1; // Slot is down for maintenance
    	$slotMaintenanceId = $this->insert($dbData);
    	return $slotMaintenanceId;
    }
    public function sanitizeParamsForInsertOrUpdate($data){
    	$dbColumns = array('slot_id', 'maintenance_date', 'maintenance_comments','release_comments');
    	$dbData = array();
    	foreach($data as $key => $value){
    		if(in_array($key, $dbColumns) && !empty($value)){
    			$dbData[$key] = $value;
    		}
    	}
    	return $dbData;
    }
    
    public function listMaintenance($fromDate, $toDate){
    	return $this->_db->select()->from($this->_name)
    						->joinLeft("slot_master", $this->_name.".slot_id = slot_master.id",array("slot"))
    						->joinLeft("courts", "slot_master.court_id = courts.id",array("court_name"))
    						->where("maintenance_date >= ?", $fromDate)->where("maintenance_date <= ?", $toDate)->query()->fetchAll();
    }
    
   public function cancelMaintenance($data){
   	$dbData = $this->sanitizeParamsForInsertOrUpdate($data);
   	$dbData['released_on'] = new Zend_Db_Expr('Now()');
   	$dbData['status'] = 0; // Slot Available
   	$this->update($dbData, array("id = ?"=>$data['maintenance_id']));
   	return $data['maintenance_id'];
   	
   }
    
    
}