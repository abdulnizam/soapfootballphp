<?php 

class STRGYM_Plugin_Routes extends Zend_Controller_Plugin_Abstract{
	private $_ACTIONS_USE_WO_LOGIN = array("login","forgotPassword","changePassword");
	
	public function preDispatch(Zend_Controller_Request_Abstract $request){
		//setting up default path
		$frontController = Zend_Controller_Front::getInstance();
		$mvcInstance = Zend_Layout::getMvcInstance();
		if(!$mvcInstance){
			Zend_Layout::startMvc();
			$mvcInstance = Zend_Layout::getMvcInstance();
		}
		
		$mvcInstance->setLayoutPath(APPLICATION_PATH.'/layouts/');
        
		
        $view = $mvcInstance->getView();
		
        $baseUrl = $view->baseURL;
        
        
        
        $view->user = (object) array();
        $view->user->loginUrl = $baseUrl."/user/login/login";
        $view->user->doLoginUrl = $baseUrl."/user/login/do-login";
        $view->user->doResetPasswordUrl = $baseUrl."/user/login/do-reset-password";
        $view->user->sendResetLinkUrl = $baseUrl."/user/login/send-reset-link";
        $view->user->resetPasswordUrl = $baseUrl."/user/login/reset-password";
        
        
		
	}
	
}

?>