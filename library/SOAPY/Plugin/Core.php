<?php 

class SOAPY_Plugin_Core extends Zend_Controller_Plugin_Abstract{
	private $_ACTIONS_USE_WO_LOGIN = array("login","forgotPassword","changePassword");
	
	public function preDispatch(Zend_Controller_Request_Abstract $request){
		//setting up default path
		$frontController = Zend_Controller_Front::getInstance();
		$mvcInstance = Zend_Layout::getMvcInstance();
		if(!$mvcInstance){
			Zend_Layout::startMvc();
			$mvcInstance = Zend_Layout::getMvcInstance();
		}
		
		$mvcInstance->setLayoutPath(APPLICATION_PATH.'/layouts/');
        
		
        $view = $mvcInstance->getView();
		
        $config = Zend_Registry::get("config");
        
        $view->version = $config['version'];
        
        
		$frontController = Zend_Controller_Front::getInstance();
		$baseURL = $config["baseHttp"].$frontController->getBaseUrl();
		$view->advanceAmount = $config['advanceAmount'];
		$view->moduleName = $request->getModuleName();
		$view->baseURL = $baseURL;
		$view->jsPath = $baseURL.'/js';
		$view->pluginsPath = $baseURL.'/plugins';
		$view->cssPath = $baseURL.'/css';
		$view->imagesPath = $baseURL.'/images';
		$view->loginUrl = $baseURL.'/user/login/login';
		$view->doLoginUrl = $baseURL.'/user/login/do-login';
		$view->logoutUrl = $baseURL.'/user/user/logout';
		$view->changePasswordUrl = $baseURL.'/user/user/change-password';
		$view->getAvailableSlots = $baseURL."/default/index/get-available-slots";
		$view->bookingAPI = $baseURL."/default/index/book";
		$view->seoScripts = self::getSeoScripts($config['title'], $config["baseHttp"].$frontController->getBaseUrl());
		
		if($request->getModuleName() == "user" && $request->getControllerName() == "login"){
			$view->dashboardUrl = $baseURL."/admin/index/index";
			$mvcInstance->setLayout('login');
		}
		else if($request->getModuleName() == "admin"){
			
			$auth = Zend_Auth::getInstance();
			
		
			$view->isAdmin = $auth->getIdentity()->is_admin == 0;
	
			$view->getAllSlotsAPI = $baseURL."/admin/index/get-all-slots";
			$view->getBookingsAPI = $baseURL."/admin/index/get-bookings";
			$view->getLiveBookingsAPI = $baseURL."/admin/index/get-live-bookings";
			$view->getLiveSlotMaintenanceAPI = $baseURL."/admin/index/get-live-slot-maintenance";
			$view->bookingsUrl = $baseURL."/admin/index/bookings";
			$view->slotManagementUrl = $baseURL."/admin/index/slot-management";
			$view->getLiveBookingsAPI = $baseURL."/admin/index/get-live-bookings";
			$view->getAllBookingsForDateAPI = $baseURL."/admin/index/get-all-bookings-for-date";
			
			
			$view->dashboardUrl = $baseURL."/admin/index/index";
			$view->maintenanceReportUrl = $baseURL."/admin/reports/maintenance";
			$view->bookingsReportUrl = $baseURL."/admin/reports/bookings";
			$view->invalidBookingsReportUrl = $baseURL."/admin/reports/invalid-bookings";
			$view->cancelledBookingsReportUrl = $baseURL."/admin/reports/cancelled-bookings";
				
			$view->maintenanceAPI = $baseURL."/admin/index/set-maintenance";
			$view->listMaintenanceAPI = $baseURL."/admin/reports/get-maintenance";
			$view->bookingsReportAPI = $baseURL."/admin/reports/get-bookings";
			$view->invalidBookingsReportAPI = $baseURL."/admin/reports/get-invalid-bookings";
			$view->cancelledBookingsReportAPI = $baseURL."/admin/reports/get-cancelled-bookings";
				
			$view->cancelMaintenanceAPI = $baseURL."/admin/index/cancel-maintenance";
			$view->cancelBookingAPI = $baseURL."/admin/index/cancel-booking";
			
			
			$mvcInstance->setLayout('admin');
		
		}
		else{
			$view->userDetails = Zend_Auth::getInstance()->getIdentity();
			
				
			
			$mvcInstance->setLayout($config['layout']);
		}
		
		
		//$mvcInstance->setLayout("maintanence");
		
		
		
	}

	public static function getSeoScripts($title, $url) {

		$script = '
					<link rel="canonical" href="'.$url.'" />
				    <meta property="og:locale" content="en_US" />
				    <meta property="og:type" content="article" />
				    <meta property="og:title" content="'.$title.'" />
				    <meta property="og:url" content="'.$url.'" />
				    <meta property="og:site_name" content="'.$title.'" />
				    <meta name="twitter:card" content="summary" />
				    <meta name="twitter:title" content="'.$title.'" />
				    
				   ';
		return $script;

	}
	
}

?>