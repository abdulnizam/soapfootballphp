# Use a base image with Ubuntu
FROM ubuntu:20.04

# Set environment variables for non-interactive installation
ENV DEBIAN_FRONTEND=noninteractive

# Add the necessary repository, update the package list, and install packages
RUN apt-get update && \
    apt-get install -y software-properties-common && \
    add-apt-repository -y ppa:ondrej/php && \
    apt-get update && \
    apt-get install -y php5.6 php5.6-mysql php5.6-mbstring php5.6-mcrypt mysql-server apache2

RUN apt-get update && apt-get install -y cron

# Copy the chennai directory to the Docker image
COPY . /var/www/html/soapfootball

# Expose the default Apache port
EXPOSE 80

# Configure Apache to serve the chennai/index.php file
RUN echo '<VirtualHost *:80>\n\
    ServerAdmin admin@soapfootballchennai.com\n\
    SetEnv APPLICATION_ENV "PROD"\n\
    DocumentRoot /var/www/html/soapfootball/chennai\n\
    <Directory /var/www/html/soapfootball/chennai>\n\
        Options Indexes FollowSymLinks MultiViews\n\
        AllowOverride All\n\
        Order allow,deny\n\
        allow from all\n\
    </Directory>\n\
</VirtualHost>' > /etc/apache2/sites-available/000-default.conf

# Enable the necessary Apache modules
RUN a2enmod rewrite

# Add the cron job configuration
COPY cronjob /etc/cron.d/my-cronjob
# RUN echo '*/5 * * * * php /var/www/html/soapfootball/chennai/UpdateChunkBookings.php schedule:run >> /var/log/cron.log 2>&1' > /etc/cron.d/my-cronjob
RUN chmod 0644 /etc/cron.d/my-cronjob

# Start the Apache server and cron service
CMD service apache2 start && cron -f