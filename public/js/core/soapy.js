var clientApp = angular.module('soapy-client',['ui.bootstrap']);
clientApp.controller('client-controller', function($scope, $http){
	
	$scope.currentDate = new Date();
	$scope.currentDate.setHours(0,0,0,0);
	$scope.yesterDayDate = new Date();
	$scope.yesterDayDate.setDate($scope.yesterDayDate.getDate()-2);
	
	$scope.currentCalendarMonth = $scope.currentDate.getMonth();
	$scope.currentCalendarYear = $scope.currentDate.getFullYear();
	
	$scope.monthsArr = ['JAN','FEB','MAR','APR','MAY','JUN','JUL','AUG','SEP','OCT','NOV','DEC'];
	
	$scope.weeksArr = [];
	
	
	$scope.$watch('currentCalendarMonth', function(newValue){
		renderCalendar();
	});
	$scope.selectedDate = '';
	$scope.$watch('selectedDate', function(newValue){
		if(newValue == ''){
			angular.element('#slot-tr').css('display','none');
		}
		else{
			angular.element('#slot-tr').css('display','table-row');
		}
	});
	
	$scope.goPrevMonth = function(){
		angular.element("#calTable").append(angular.element("#slot-tr"));
		$scope.selectedDate = '';
		if($scope.currentCalendarMonth == 0){
			$scope.currentCalendarYear--;
			$scope.currentCalendarMonth = 11;
		}
		else{
			$scope.currentCalendarMonth--;
		}
		
	}
	
	$scope.goNextMonth = function(){
		angular.element("#calTable").append(angular.element("#slot-tr"));
		$scope.selectedDate = '';
		if($scope.currentCalendarMonth == 11){
			$scope.currentCalendarYear++;
			$scope.currentCalendarMonth = 0;
		}
		else{
			$scope.currentCalendarMonth++;
		}
		
	}
	
	$scope.selectDate = function($event, selectedDate){
		$scope.availableSlots = [];
		$scope.slotLoading = true;
		if(selectedDate > $scope.yesterDayDate){
			if($scope.selectedDate == selectedDate){
				$scope.selectedDate = '';
			}
			else{
				$scope.selectedDate = selectedDate;
				angular.element($event.currentTarget).parent().after(angular.element("#slot-tr"));
				/* ajax call needs to be called for getting available slots */
				if($scope.selectedDate && $scope.selectedDate != ''){
					$http.get(GET_AVAILABLE_SLOTS_API+"?date="+$scope.selectedDate.yyyymmdd()).then(function(res){
						if(res.data.status == "success"){
							$scope.slotLoading = false;
							var slots = res.data.slots;
							angular.forEach(slots, function(courtSlots, courtId){
								angular.forEach(courtSlots.slots, function(slot){
									angular.forEach($scope.cart.items, function(item){
										if(item.slot.id == slot.id && item.date.getTime() == $scope.selectedDate.getTime()){
											slot.selected = true;
											return false;
										}
									});
								});
								
							});
							$scope.availableSlots = slots;
						}
					});
				}
				
			}
		}
		else{
			$scope.selectedDate = '';
		}
		
	}
	
	$scope.openModal = function(slot){
		$scope.selectedSlot = slot;
		$("#book").modal("show");
		
	}
	$scope.openCartModal = function(slot){
		$scope.selectedSlot = slot;
		$("#cartModal").modal("show");
		
	}
	
	$scope.selectSlot = function(slot){
		if(slot.is_booked == '0' && slot.is_maintenance == '0'){
			if(!slot.selected){
				slot.selected = true;
				var cartItem = {};
				cartItem.date = new Date($scope.selectedDate);
				cartItem.slot = angular.extend({},slot);
				cartItem.is_booked_combo_offer = 0;
				if(slot.isPublicHoliday || $scope.selectedDate.getDay() == 0 || $scope.selectedDate.getDay() == 6 ){
					cartItem.amount = parseInt(slot.weekend_tariff);
				}
				else{
					cartItem.amount = parseInt(slot.normal_tariff);
				}
				
				$scope.cart.items.push(cartItem);
				if(!$scope.cartSlots[cartItem.slot.court_id]){
					$scope.cartSlots[cartItem.slot.court_id] = [];
				}
				$scope.cartSlots[cartItem.slot.court_id].push(cartItem.slot.slot)
			}
			else{
				angular.forEach($scope.cart.items, function(item, index){
					if(item.slot.id == slot.id && item.date.getTime() == $scope.selectedDate.getTime()){
						$scope.cart.items.splice(index,1);
						slot.selected = false;
						return false;
					}
				});
			}
			updateCartItemPrice();
			
		}
	}
	
	function updateCartItemPrice(){
		updateCartSlots();
		console.log("cartSlots", $scope.cartSlots);
		angular.forEach($scope.cart.items, function(item,index){
			var slot = item.slot;
			item.is_booked_combo_offer = 0;
			if(item.slot.combo_offer_slots){
				var comboOfferPrice = {};
				angular.forEach(item.slot.combo_offer.split(";"), function(offerPriceCourtWise){
					var courtPriceArr = offerPriceCourtWise.split(":");
					comboOfferPrice[courtPriceArr[0]] = courtPriceArr[1];
				});
				var offerCourtDetails = item.slot.combo_offer_slots.split(';');
				for(var i =0; i<offerCourtDetails.length; i++){
					var courtSlots = offerCourtDetails[i].split(':');
					if(isSlotAvailableInCart(item.slot, courtSlots[0], courtSlots[1])){
						if(comboOfferPrice[courtSlots[0]].indexOf('%') != -1){
							var percentage = comboOfferPrice[courtSlots[0]];
							percentage = percentage.substring(0, percentage.length - 1);
							if(item.slot.isPublicHoliday || item.date.getDay() == 0 || item.date.getDay() == 6 ){
								item.amount = parseFloat(parseInt(slot.weekend_tariff) * parseInt(percentage) / 100);
							}
							else{
								item.amount = parseFloat(parseInt(slot.normal_tariff) * parseInt(percentage) / 100);
							}
						}
						else{
							if(parseInt(comboOfferPrice[courtSlots[0]]) > 0){
								item.amount = parseInt(comboOfferPrice[courtSlots[0]]);
							}
							else{
								if(item.slot.isPublicHoliday || item.date.getDay() == 0 || item.date.getDay() == 6 ){
									item.amount = parseInt(slot.weekend_tariff) + parseInt(comboOfferPrice[courtSlots[0]]);
								}
								else{
									item.amount = parseInt(slot.normal_tariff) + parseInt(comboOfferPrice[courtSlots[0]]);
								}
							}
						}
						item.is_booked_combo_offer = 1;
						item.combo_offer = comboOfferPrice[courtSlots[0]];
						break;
					}
				}
				if(item.is_booked_combo_offer == 0){
					delete item.combo_offer;
					if(item.slot.isPublicHoliday || item.date.getDay() == 0 || item.date.getDay() == 6 ){
						item.amount = parseInt(slot.weekend_tariff);
					}
					else{
						item.amount = parseInt(slot.normal_tariff);
					}
					
				}
			}
		});
	}
	function isSlotAvailableInCart(slot, courtId, slots){
		if(slots == 'all'){ 
			if($scope.cartSlots[courtId] && $scope.cartSlots[courtId].length > 0){
				var refSlot;
				for(var i = 0; i<$scope.cartSlots[slot.court_id].length;i++){
					if(slot.slot == $scope.cartSlots[slot.court_id][i].slot){
						refSlot = $scope.cartSlots[slot.court_id][i];
						break;
					}
				}
					for(var j = 0; j<$scope.cartSlots[courtId].length; j++){
						if($scope.cartSlots[courtId][j].slot == refSlot.slot && courtId == refSlot.court_id){
							break;
						}
						if($scope.cartSlots[courtId][j].offAppliedCourts.indexOf(refSlot.court_id) == -1 && refSlot.offAppliedCourts.indexOf(courtId) == -1){
							refSlot.offAppliedCourts.push(courtId);
							if(refSlot.court_id == '3' && courtId == '2'){
								refSlot.offAppliedCourts.push(refSlot.court_id);
							}
							$scope.cartSlots[courtId][j].offAppliedCourts.push(refSlot.court_id);
							if(courtId == '3' && refSlot.court_id == '2'){
								$scope.cartSlots[courtId][j].offAppliedCourts.push(courtId);
							}
							
							return true;
						}
					}
				
			}
			return false;
		}
		var slotsArr = slots.split(',');
		for(var i=0; i<slotsArr.length; i++){
			for(var j = 0; j<$scope.cartSlots[courtId].length; j++){
				if($scope.cartSlots[courtId][j].slot == slotsArr[i]){
					if($scope.cartSlots[courtId][j].slot != refSlot.slot && $scope.cartSlots[courtId][j].offAppliedCourts.indexOf(refSlot.court_id) == -1){
						refSlot.offAppliedCourts.push(courtId);
						if(refSlot.court_id == '2' && courtId == '1'){
							refSlot.offAppliedCourts.push(refSlot.court_id);
						}
						$scope.cartSlots[courtId][j].slot.offAppliedCourts.push(refSlot.court_id);
						if($scope.cartSlots[courtId][j].court_id == '2' && courtId == '1'){
							$scope.cartSlots[courtId][j].offAppliedCourts.push($scope.cartSlots[courtId][j].court_id);
						}
						return true;
					}
				}
			}
		}
		
		return false;
		
	}
	function updateCartSlots(){
		var cartSlots = {};
		angular.forEach($scope.cart.items, function(item){
			if(!cartSlots[item.slot.court_id]){
				cartSlots[item.slot.court_id] = [];
			}
			var slot = angular.merge({}, item.slot);
			slot.offAppliedCourts = [];
			cartSlots[item.slot.court_id].push(slot);
		});
		$scope.cartSlots = cartSlots;
	}
	
	$scope.cart = {items:[]};
	$scope.cartSlots = {};
	$scope.comboSlots = [];
	$scope.$watch('cart', function(newValue){
		$scope.cart.totalAmount = 0;
		$scope.cart.totalSlots = 0;
		angular.forEach($scope.cart.items, function(item){
			$scope.cart.totalAmount +=  item.amount;
			$scope.cart.totalSlots++;
		});
		$scope.cart.advancePayment = $scope.cart.totalSlots * ADVANCE_AMOUNT;
		$scope.cart.totalAmountWithGST = $scope.cart.totalAmount + ($scope.cart.totalAmount*(18/100));
		$scope.cart.balance = $scope.cart.totalAmount- $scope.cart.advancePayment;
	},true);
	
	$scope.removeCartItem = function(item, index){
		angular.forEach($scope.availableSlots, function(courtSlots){
			angular.forEach(courtSlots.slots, function(slot){
				if(slot.id == item.slot.id && item.date.getTime() == $scope.selectedDate.getTime()){
					slot.selected = false;
				}
			});
		});
		$scope.cart.items.splice(index,1);
		if($scope.cart.items.length == 0){
			$("#cartModal").modal("hide");
		}
		else{
			updateCartItemPrice();
		}
	}
	$scope.removeCartItemCourt = function(item){
		
	}
	
	
	$scope.showBookingPopup = function(){
		$("#cartModal").modal("hide");
		setTimeout(function(){
			$("#book").modal("show");
		}, 500)
		
	}
	
	$scope.book = function(){
		$scope.booking = '';
		
		var postData = {};
		postData['bookingDetails'] = $scope.bookingDetails;
		
		postData['cartDetails'] = [];
		angular.forEach($scope.cart.items, function(cartItem){
			var item = {};
			item['slot_id'] = cartItem['slot']['id'];
			item['date'] = cartItem['date'].yyyymmdd();
			item['is_booked_combo_offer'] = cartItem['is_booked_combo_offer'];
			item['combo_offer'] = cartItem['combo_offer'];
			postData['cartDetails'].push(item);
		});
		$http({
			url:BOOKING_API,
			method:"post",
			data:postData
		}).then(function(res){
			if(res.data.status="success"){
				window.location.href = PAYMENT_REQUEST_HANDLER+"?orderId="+res.data.orderId;
				/*$scope.booking = angular.merge({}, $scope.cart);
				$scope.booking.orderId = res.data.orderId;
				$scope.booking.bookingDetails = $scope.bookingDetails;
				$scope.cart = {items:[]};
				$scope.selectedDate = '';
				$scope.selectedSlot = '';*/
				$("#book").modal("hide");
				//window.location.hash="bookedDetails";
			}
		});
	}
	
	function renderCalendar(){
		  var date = new Date($scope.currentCalendarYear, $scope.currentCalendarMonth, 1);
		  var dateArr = [];
	      while (date.getMonth() == $scope.currentCalendarMonth ) {
	    	
	    	if(date.getDate() == 1){
	    		var dayIndex = date.getDay();
	    		dayIndex = dayIndex == 0? 7 : dayIndex;
	    		for(var i = 0; i < dayIndex-1; i++){
	    			dateArr.push('');
	    		}
	    	}
	    	dateArr.push(new Date(date));
	    	date.setDate(date.getDate()+1);
	      }	
		  $scope.weeksArr = chunk(dateArr, 7);
		  
	}
	function chunk(arr, size) {
		  var newArr = [];
		  for (var i=0; i<arr.length; i+=size) {
		    newArr.push(arr.slice(i, i+size));
		  }
		  var lastWeekArr = newArr[newArr.length-1];
		  if(lastWeekArr.length < size){
			  var fillCount = size - lastWeekArr.length;
			  for(var i = 0; i < fillCount; i++){
				  lastWeekArr.push('');
			  }
		  }
		  return newArr;
		}
	
});