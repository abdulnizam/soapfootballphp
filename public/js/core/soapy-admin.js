var adminApp = angular.module('soapy-admin',['angular-rating-icons']);
adminApp.controller('dashboard', function($scope, $http){
	
	
	$scope.currentDate = new Date();
	$scope.yesterdayDate = new Date();
	$scope.yesterdayDate.setDate($scope.yesterdayDate.getDate()-1);
	
	$scope.currentCalendarMonth = $scope.currentDate.getMonth();
	$scope.currentCalendarYear = $scope.currentDate.getFullYear();
	
	$scope.monthsArr = ['JAN','FEB','MAR','APR','MAY','JUN','JUL','AUG','SEP','OCT','NOV','DEC'];
	
	$scope.weeksArr = [];
	
	
	$scope.$watch('currentCalendarMonth', function(newValue){
		renderCalendar();
	});
	$scope.selectedDate = '';
	$scope.$watch('selectedDate', function(newValue){
		if(newValue == ''){
			angular.element('#slot-tr').css('display','none');
		}
		else{
			angular.element('#slot-tr').css('display','table-row');
		}
	});
	
	$scope.goPrevMonth = function(){
		angular.element("#calTable").append(angular.element("#slot-tr"));
		$scope.selectedDate = '';
		if($scope.currentCalendarMonth == 0){
			$scope.currentCalendarYear--;
			$scope.currentCalendarMonth = 11;
		}
		else{
			$scope.currentCalendarMonth--;
		}
		
	}
	
	$scope.goNextMonth = function(){
		angular.element("#calTable").append(angular.element("#slot-tr"));
		$scope.selectedDate = '';
		if($scope.currentCalendarMonth == 11){
			$scope.currentCalendarYear++;
			$scope.currentCalendarMonth = 0;
		}
		else{
			$scope.currentCalendarMonth++;
		}
		
	}
	
	$scope.selectDate = function($event, selectedDate){
		$scope.availableSlots = [];
		
		
			if($scope.selectedDate == selectedDate){
				$scope.selectedDate = '';
			}
			else{
				$scope.selectedDate = selectedDate;
				angular.element($event.currentTarget).parent().after(angular.element("#slot-tr"));
				/* ajax call needs to be called for getting available slots */
				if($scope.selectedDate && $scope.selectedDate != ''){
					
						updateSlotDetails();
					
				}
				
			}
		
	}
	
	function updateSlotDetails(){
		$http.get(GET_ALL_SLOTS_API+"?date="+$scope.selectedDate.yyyymmdd()).then(function(res){
			$scope.slotLoading = true;
			if(res.data.status == "success"){
				$scope.slotDetails = res.data.slotDetails;
				$scope.totalCourts = 0;
				angular.forEach($scope.slotDetails, function(courtSlots){
					$scope.totalCourts++;
				});
			}
			$scope.slotLoading = false;
		});
	}
	
	$scope.selectSlotForBooking = function(slot){
		$scope.selectedSlot = slot;
		$("#book").modal("show");
	}
	
	
	$scope.selectSlotForMaintenance = function(slot){
		$scope.selectedSlot = slot;
		$("#maintenance").modal("show");
	}
	
	$scope.maintenance = function(){
		var maintenanceDetails = $scope.maintenanceDetails;
		maintenanceDetails['maintenance_date'] = $scope.selectedDate.yyyymmdd();
		maintenanceDetails['slot_id'] = $scope.selectedSlot.id;
		$http({
			url:MAINTENANCE_API,
			method:'post',
			data:maintenanceDetails
		}).then(function(res){
			if(res.data.status == "success"){
				updateSlotDetails();
				$("#maintenance").modal("hide");
			}
		});
	}
	
	
	$scope.book = function(){
		$scope.booking = '';
		var postData = {};
		var bookingDetails = $scope.bookingDetails;
		bookingDetails.booking_advance = 1000;
		bookingDetails['booked_by'] = 1;
		var cartItems = [];
		var cartItem = {};
		cartItem.date = $scope.selectedDate.yyyymmdd();
		cartItem.slot_id = $scope.selectedSlot.id;
		cartItems.push(cartItem);
		postData['cartDetails'] = cartItems;
		postData['bookingDetails'] = bookingDetails;
		/*postData['booking_date'] = $scope.selectedDate.yyyymmdd();
		postData['slot_id'] = $scope.selectedSlot.id;*/
		
		$http({
			url:BOOKING_API,
			method:"post",
			data:postData
		}).then(function(res){
			if(res.data.status="success"){

				updateSlotDetails();
				$("#book").modal("hide");
				
			}
		});
	}
	$scope.showbookingDetails = function(bookingDetails){
		$scope.selectedBooking = bookingDetails;
		$("#booking-view").modal("show");
	}
	
	$scope.showCancelMaintenance = function(slot){
		$scope.selectedSlot = slot;
		$("#cancel-maintenance").modal("show");
	}
	$scope.showCancelBooking = function(slot){
		$scope.selectedSlot = slot;
		$("#cancel-booking").modal("show");
	}
	$scope.cancelMaintenance = function(){
		var maintenanceDetails = $scope.maintenanceDetails;
		maintenanceDetails['maintenance_date'] = $scope.selectedDate.yyyymmdd();
		maintenanceDetails['slot_id'] = $scope.selectedSlot.id;
		maintenanceDetails['maintenance_id'] = $scope.selectedSlot.maintenance_id;
		
		$http({
			url:CANCEL_MAINTENANCE_API,
			method:'post',
			data:maintenanceDetails
		}).then(function(res){
			if(res.data.status == "success"){
				updateSlotDetails();
				$("#cancel-maintenance").modal("hide");
			}
		});
	}
	
	$scope.cancelBooking = function(){
		var bookingDetails = $scope.bookingDetails;
		bookingDetails['booking_id'] = $scope.selectedSlot.booking_id;
		
		$http({
			url:CANCEL_BOOKING_API,
			method:'post',
			data:bookingDetails
		}).then(function(res){
			if(res.data.status == "success"){

				updateSlotDetails();
				$("#cancel-booking").modal("hide");
			}
		});
	}
	
	
	function renderCalendar(){
		  var date = new Date($scope.currentCalendarYear, $scope.currentCalendarMonth, 1);
		  var dateArr = [];
	      while (date.getMonth() == $scope.currentCalendarMonth ) {
	    	
	    	if(date.getDate() == 1){
	    		var dayIndex = date.getDay();
	    		dayIndex = dayIndex == 0?7:dayIndex;
	    		for(var i = 0; i < dayIndex-1; i++){
	    			dateArr.push('');
	    		}
	    	}
	    	dateArr.push(new Date(date));
	    	date.setDate(date.getDate()+1);
	      }	
		  $scope.weeksArr = chunk(dateArr, 7);
		  
	}
	function chunk(arr, size) {
		  var newArr = [];
		  for (var i=0; i<arr.length; i+=size) {
		    newArr.push(arr.slice(i, i+size));
		  }
		  var lastWeekArr = newArr[newArr.length-1];
		  if(lastWeekArr.length < size){
			  var fillCount = size - lastWeekArr.length;
			  for(var i = 0; i < fillCount; i++){
				  lastWeekArr.push('');
			  }
		  }
		  return newArr;
		}
	
});

adminApp.controller('bookings', function($scope, $http){
	
	$http.get(GET_ALL_SLOTS_API).then(function(res){
		$scope.slots = res.data.slots;
	});
	
	$http.get(GET_LIVE_BOOKINGS_API).then(function(res){
		$scope.bookings = res.data.bookings;
	});
});

adminApp.controller('slot-management', function($scope, $http){
	
	$http.get(GET_ALL_SLOTS_API).then(function(res){
		$scope.slots = res.data.slots;
	});
	
	$http.get(GET_SLOT_MAINTENANCE_API).then(function(res){
		$scope.slotMaintenanceList = res.data.slotMaintenanceList;
	});
});


adminApp.controller('maintenance-report', function($scope, $http){
		
	$scope.maintenanceList = [];
	
	$scope.$watch("fromDate", function(fromDate){
		filterMaintenanceList();
	});
	$scope.$watch("toDate", function(toDate){
		filterMaintenanceList();
	});
	
	function filterMaintenanceList(){
		$scope.maintenanceList = [];
		console.log("fromDate", $scope.fromDate);
		console.log("toDate", $scope.toDate);
		
		if($scope.fromDate && $scope.toDate){
			$http.get(MAINTENANCE_API+"?fromDate="+$scope.fromDate.yyyymmdd()+"&toDate="+$scope.toDate.yyyymmdd()).then(function(res){
				$scope.maintenanceList = res.data.maintenanceList;
			});
		}
	}
	
});

adminApp.controller('bookings-report', function($scope, $http){
	
	$scope.bookings = [];

	$scope.fromDate = new Date();
	$scope.toDate = new Date();
	
	$scope.$watch("fromDate", function(fromDate){
		filterBookings();
	});
	$scope.$watch("toDate", function(toDate){
		filterBookings();
	});
	
	function filterBookings(){
		$scope.maintenanceList = [];
		if($scope.fromDate && $scope.toDate){
			$http.get(GET_BOOKINGS_API+"?fromDate="+$scope.fromDate.yyyymmdd()+"&toDate="+$scope.toDate.yyyymmdd()).then(function(res){
				$scope.bookings = res.data.bookings;
				$scope.totalManualBookings = 0;
				$scope.totalCancelledBookings = 0;
				$scope.totalOnlineBookings = 0;
				$scope.totalInvalidBookings = 0;
				$scope.totalBookings = 0;
				angular.forEach($scope.bookings, function(booking){
					if(booking.booked_by == '1'){
						if(booking.status == '1'){
							$scope.totalManualBookings++;
							$scope.totalBookings++;
						}
						else if(booking.status == '10'){
							$scope.totalCancelledBookings++;
						}
					}
					else if (booking.booked_by == '0'){
						if(booking.status == '1'){
							$scope.totalOnlineBookings++;
							$scope.totalBookings++;
						}
						else{
							if(booking.status == '2' || booking.status == '3'){
								$scope.totalInvalidBookings++;
							}
						}
					}
					
					
				});
			});
		}
	}
	
});

adminApp.controller('invalid-bookings-report', function($scope, $http){
	
	$scope.bookings = [];

	$scope.fromDate = new Date();
	$scope.toDate = new Date();
	
	$scope.$watch("fromDate", function(fromDate){
		filterBookings();
	});
	$scope.$watch("toDate", function(toDate){
		filterBookings();
	});
	
	function filterBookings(){
		$scope.maintenanceList = [];
		if($scope.fromDate && $scope.toDate){
			$http.get(GET_INVALID_BOOKINGS_API+"?fromDate="+$scope.fromDate.yyyymmdd()+"&toDate="+$scope.toDate.yyyymmdd()).then(function(res){
				$scope.bookings = res.data.bookings;
			});
		}
	}
	
});
adminApp.controller('cancelled-bookings-report', function($scope, $http){
	
	$scope.bookings = [];
	$scope.fromDate = new Date();
	$scope.toDate = new Date();
	$scope.$watch("fromDate", function(fromDate){
		filterBookings();
	});
	$scope.$watch("toDate", function(toDate){
		filterBookings();
	});
	
	function filterBookings(){
		$scope.maintenanceList = [];
		if($scope.fromDate && $scope.toDate){
			$http.get(GET_CANCELLED_BOOKINGS_API+"?fromDate="+$scope.fromDate.yyyymmdd()+"&toDate="+$scope.toDate.yyyymmdd()).then(function(res){
				$scope.bookings = res.data.bookings;
			});
		}
	}
	
});


adminApp.controller("login", function($scope, $http){
	$scope.login = function(){
		$scope.checked = true;
		$http({
			url: DO_LOGIN_URL,
			method:"post",
			data:{username:$scope.username, password:$scope.password}
		}).then(function(res){
			if(res.data.status == "success"){
				window.location.href = res.data.redirectUrl;
			}
			else{
				$scope.errorMessage = res.data.errorMsg;
				$scope.hasError = true;
				
			}
		});
		$scope.errorAnimate = '';
	}
	
});
adminApp.controller("feedback", function($scope, $http, $timeout){
	$scope.slots = [];
	$scope.feedbackDetails = {"overall_rating" : 0} ;
	$http.get(GET_ALL_SLOTS_API).then(function(res){
		angular.forEach(res.data.slotDetails, function(courts){
			angular.forEach(courts.slots, function(slot){
				$scope.slots.push(slot.slot);
			});
		});
	});
	$scope.questions = QUESTIONS;
	$scope.$watch("feedbackDetails.feedback.questions", function(newValue){
		if($scope.feedbackDetails.feedback){
			var ratingCount = 0;
			var overAllRatings = 0;
			angular.forEach($scope.feedbackDetails.feedback.questions, function(ratings){
				overAllRatings += ratings;
				ratingCount++;
			});
			$scope.feedbackDetails.overall_rating = parseFloat(overAllRatings / ratingCount).toFixed(2);
		}
	}, true);
	
	$scope.submit = function(){
		$http({
			url:SAVE_API,
			method:"post",
			data:$scope.feedbackDetails
		}).then(function(res){
			if(res.data.status == "success"){
				$scope.successMessage = "Thanks for giving your valuable feedback.";
				angular.forEach($scope.feedbackDetails.feedback.questions, function(ratings, key){
					$scope.feedbackDetails.feedback.questions[key] = 0;
				});
				$scope.feedbackDetails.name = '';
				$scope.feedbackDetails.email = '';
				$scope.feedbackDetails.phone = '';
				$scope.feedbackDetails.comments = '';
			}
			else{
				$scope.errorMessage = "Some problem occured. Please try again.";
			}
			document.body.scrollTop = document.documentElement.scrollTop = 0;
			$timeout(function(){
				$scope.successMessage = false;
				$scope.errorMessage = false;
			}, 5000)
			
		});
	}
});