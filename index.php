<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="Soap FootBall Hyderabad">
    <meta name="author" content="Freniz Software Solutions">
    <link rel="icon" href="../../favicon.ico">

    <title>Soap FootBall Hyderabad</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="http://getbootstrap.com/examples/sticky-footer/sticky-footer.css
">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <style type="text/css">
        article {
            display: block;
            text-align: left;
            width: 650px;
            margin: 16px 12px 12px 16px;
        }
        
        article p {
            font-size: 20px;
        }
        
        .bs-service {
            min-width: 200px;
            margin: 20px;
        }
    </style>
</head>

<body>

    <!-- Begin page content -->
    <div class="container">
        <div class="page-header">
            <h1>Soap FootBall Hyderabad</h1>
        </div>

        <div class="bs-service">
            <img src="http://www.soapfootballhyderabad.com/images/logo.png" style="width:400px" />
            <div class="row">

                <article>
                    <div>
                        <p>Websites Under Maintenance.</p>
                        <p>&mdash; The Team</p>
                    </div>
                </article>

            </div>

        </div>

    </div>

    <footer class="footer">
        <div class="container">
            <p class="text-muted">Powered By <a href="http://www.freniz.com">Freniz Software Solutions</a></p>
        </div>
    </footer>

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
</body>

</html>
