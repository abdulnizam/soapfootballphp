#!/bin/bash

DOCKER_IMAGE="$1"
DOCKER_IMAGE_TAG="$2"

CONTAINER_IDS=$(docker ps -a -q --filter "expose=3000" --format="{{.ID}}")

if [ -n "$CONTAINER_IDS" ]; then
  docker stop $CONTAINER_IDS
  docker rmi $CONTAINER_IDS
else
  echo "No containers found for the specified image."
fi
sudo docker run -d -p 3000:80 docker.io/$DOCKER_IMAGE:$DOCKER_IMAGE_TAG


