<?php
require_once 'Zend/Loader/Autoloader.php';

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{
    
    protected function _initAutoloader()
    {
        $autoloader = Zend_Loader_Autoloader::getInstance();
        $autoloader->registerNamespace('SOAPY_');
        return $autoloader;
    }
    
    public function _initConfigAndDB(){
        //$db = new Zend_Db_Adapter_Pdo_Mysql(array("dbname"=>"IPDDESK","username"=>"root","password"=>"","host"=>"localhost"));
        $config = (new Zend_Config_Ini(CONFIG_PATH."/config.ini", APPLICATION_ENV))->toArray();
        $mailConfig = (new Zend_Config_Ini(CONFIG_PATH."/mail.config.ini", APPLICATION_ENV))->toArray();
        Zend_Registry::set("config", $config);
        Zend_Registry::set("mail_config", $mailConfig);
        $db = new Zend_Db_Adapter_Pdo_Mysql($config['db']);
        $registry = Zend_Registry::getInstance();
        $registry->set("defaultDB", $db);
    }
    
    protected function _initSyncQueue(){
    	
    }
    
    protected function _initPlugins()
    {
        $this->bootstrap('FrontController');
        $front = $this->getResource('FrontController');
        $front->registerPlugin(new SOAPY_Plugin_Login());
        $front->registerPlugin(new SOAPY_Plugin_Core());
        //$front->registerPlugin(new STRGYM_Plugin_Routes());
        
    }
    
    protected function _initMail()
    {
    	try {
    		
    		$mailConfig = Zend_Registry::get("mail_config");
        
    		$mailTransport = new Zend_Mail_Transport_Smtp('smtp.gmail.com', $mailConfig['mail']['login']);
    		Zend_Mail::setDefaultTransport($mailTransport);
    	} catch (Zend_Exception $e){
    		//Do something with exception
    	}
    } 
    
}

