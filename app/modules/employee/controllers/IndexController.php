<?php


class Employee_IndexController extends Zend_Controller_Action {
	
	/**
	 * @var STRGYM_Model_Employees
	 */
	protected $_employeeModel;
	
	public function init(){
		
		$this->_employeeModel = STRGYM_Model_Employees::getInstance();
		
	}
	
	public function indexAction(){
		
		
		
	}
	
	public function listAction(){
		
		echo json_encode(array("status"=>"success","employees"=>$this->_employeeModel->listEmployees()));
		
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
	}
	
	public function getAction(){
		$employeeId = $this->_request->getParam("id");
		echo json_encode(array("status"=>"success","employeeDetails"=>$this->_employeeModel->getEmployeeDetails($employeeId)));
		
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
	}
	
	public function addEmployeeAction(){
		$employeeId = $this->_request->getParam("id");
		$this->view->employeeId = $employeeId;
	}
	
	public function saveAction(){
		
		
		$rawBody = $this->_request->getRawBody();
		$postData = Zend_Json::decode($rawBody, Zend_Json::TYPE_ARRAY);
		$employeeDetails = $postData["employeeDetails"];
		$adminIdentity = Zend_Auth::getInstance()->getIdentity();
		$adminEmail = $adminIdentity->employee_email;
		
		if(!empty($employeeDetails["id"])){
			$employeeId = $this->_employeeModel->save($employeeDetails, $adminEmail);		
		}
		else{
			$password = $this->generatePassword();
			$employeeDetails["staar_password"] = md5($password);
			$employeeId = $this->_employeeModel->save($employeeDetails, $adminEmail);
			
			$mailData = array();
			$mailData['user_name'] = $employeeDetails['employee_email'];
			$mailData['password'] = $password;
			
			STRGYM_Helper_MailHelper::sendEmployeeAddMail($employeeDetails['employee_email'], $mailData);
			
		}
		echo json_encode(array("status"=>"success", "redirectUrl"=>$this->view->employee->listUrl));
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		
	}
	
	public function deleteUserAction(){
		$userid = $this->_request->getParam("userId");
		echo json_encode(array("status"=>"success", "data"=>$this->_employeeModel->deleteEmployee($userid)));

		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
	}
	
	private function generatePassword(){
		$letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890-@#$";
		$password = "";
		
		while(strlen($password) != 15){
			$randVal = (rand(10, 99999999))%66;
			$password .= $letters[$randVal];
		}
		return $password;
	}
	
}

