<?php

class Employee_AttendanceController extends Zend_Controller_Action
{

	/**
	 * 
	 * @var STRGYM_Model_EmployeeAttendance
	 */
	protected $_attendanceModel;
	
    public function init()
    {
        /* Initialize action controller here */
    	$this->_attendanceModel = STRGYM_Model_EmployeeAttendance::getInstance();
    }

    public function indexAction()
    {
        // action body
    }
    
    public function listAction(){
    	echo json_encode(array("status" => "success", "entries"=>$this->_attendanceModel->listEntries($this->_request->getParam("date"))));
    	$this->_helper->viewRenderer->setNoRender();
    	$this->_helper->layout()->disableLayout();
    }


}

