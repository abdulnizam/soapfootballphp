<?php
class Admin_ReportsController extends Zend_Controller_Action{
	
	public function init(){
		
	}
	
	public function maintenanceAction(){
		
	}
	public function getMaintenanceAction(){
		$fromDate = $this->_request->getParam("fromDate");
		$toDate = $this->_request->getParam("toDate");
		$slotMaintenanceModel = SOAPY_Model_SlotMaintenance::getInstance();
		echo json_encode(array("status"=>"success", "maintenanceList"=>$slotMaintenanceModel->listMaintenance($fromDate, $toDate)));
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		
		
	}
	public function bookingsAction(){
		
	}
	public function getBookingsAction(){
		$fromDate = $this->_request->getParam("fromDate");
		$toDate = $this->_request->getParam("toDate");
		$bookingsModel = SOAPY_Model_Bookings::getInstance();
		
		echo json_encode(array("status"=>"success", "bookings"=>$bookingsModel->getAllBookings($fromDate, $toDate, array(1))));
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
	
	
	}
	
	public function invalidBookingsAction(){
		
	}
	
	public function getInvalidBookingsAction(){
		
		$fromDate = $this->_request->getParam("fromDate");
		$toDate = $this->_request->getParam("toDate");
		$bookingsModel = SOAPY_Model_Bookings::getInstance();
		
		echo json_encode(array("status"=>"success", "bookings"=>$bookingsModel->getAllBookings($fromDate, $toDate, array(2,3))));
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		
	}
	public function cancelledBookingsAction(){
		
	}
	
	public function getCancelledBookingsAction(){
	
		$fromDate = $this->_request->getParam("fromDate");
		$toDate = $this->_request->getParam("toDate");
		$bookingsModel = SOAPY_Model_Bookings::getInstance();
	
		echo json_encode(array("status"=>"success", "bookings"=>$bookingsModel->getAllBookings($fromDate, $toDate, array(10))));
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
	
	}
	
	
	
	public function paymentsAction(){
		
	}
	
}