<?php

class Admin_IndexController extends Zend_Controller_Action
{

	
    public function init()
    {
        /* Initialize action controller here */
    }

	public function indexAction()
    {
        // action body
        
    	
    	$this->view->totalCourts = 2;
        
    }
	public function bookingsAction()
    {
        // action body
        
    }
    
    public function getBookingsAction(){
    	
    	$fromDate = $this->_request->getParam("fromDate");
    	$toDate = $this->_request->getParam("toDate");
    	$bookingsModel = SOAPY_Model_Bookings::getInstance();
    	$bookings = $bookingsModel->getAllBookings($fromDate,$toDate);
    	echo json_encode(array("status"=>"success","bookings"=>$bookings));
    	$this->_helper->viewRenderer->setNoRender();
    	$this->_helper->layout()->disableLayout();
    }
	public function getLiveBookingsAction(){
    	 
    	$bookingsModel = SOAPY_Model_Bookings::getInstance();
    	$bookings = $bookingsModel->getAllLiveBookings();
    	echo json_encode(array("status"=>"success","bookings"=>$bookings));
    	$this->_helper->viewRenderer->setNoRender();
    	$this->_helper->layout()->disableLayout();
    }
    
    public function getAllSlotsAction(){
    	$slotsModel = SOAPY_Model_SlotMaster::getInstance();
    	$date = $this->_request->getParam("date");
    	echo json_encode(array("status"=>"success","slotDetails"=>$slotsModel->getAllSlotsWithDetails($date)));
    	$this->_helper->viewRenderer->setNoRender();
    	$this->_helper->layout()->disableLayout();
    }
    public function bookingReportAction(){
    	
    }
    
    public function slotManagementAction(){
    	
    }
    
    public function getLiveSlotMaintenanceAction(){
    	$slotMaintenanceModel = SOAPY_Model_SlotMaintenance::getInstance();
    	$slotMaintenanceList = $slotMaintenanceModel->getLiveSlotMaintenanceList();
    	echo json_encode(array("status"=>"success","slotMaintenanceList"=>$slotMaintenanceList));
    	$this->_helper->viewRenderer->setNoRender();
    	$this->_helper->layout()->disableLayout();
    }
	public function setMaintenanceAction(){
    
    	$rawBody = $this->_request->getRawBody();
    	$postData = Zend_Json::decode($rawBody, Zend_Json::TYPE_ARRAY);
    	
    	/* $date = $this->_request->getParam("date");
    	$slotId = $this->_request->getParam("slot");
    	$data['maintenance_date'] = $date;
    	$data['slot_id'] = $slotId; */
    	$slotMaintenanceModel = SOAPY_Model_SlotMaintenance::getInstance();
    	$slotMaintenanceId = $slotMaintenanceModel->save($postData);
    	echo json_encode(array("status"=>"success","slotMaintenanceId"=>$slotMaintenanceId));
    	$this->_helper->viewRenderer->setNoRender();
    	$this->_helper->layout()->disableLayout();
    }
    
public function cancelMaintenanceAction(){
    
    	$rawBody = $this->_request->getRawBody();
    	$postData = Zend_Json::decode($rawBody, Zend_Json::TYPE_ARRAY);
    	 
    	/* $date = $this->_request->getParam("date");
    	 $slotId = $this->_request->getParam("slot");
    	 $data['maintenance_date'] = $date;
    	 $data['slot_id'] = $slotId; */
    	$slotMaintenanceModel = SOAPY_Model_SlotMaintenance::getInstance();
    	$slotMaintenanceId = $slotMaintenanceModel->cancelMaintenance($postData);
    	echo json_encode(array("status"=>"success","slotMaintenanceId"=>$slotMaintenanceId));
    	$this->_helper->viewRenderer->setNoRender();
    	$this->_helper->layout()->disableLayout();
    }
    
	public function cancelBookingAction(){
    
    	$rawBody = $this->_request->getRawBody();
    	$postData = Zend_Json::decode($rawBody, Zend_Json::TYPE_ARRAY);
    	 
    	$bookingModel = SOAPY_Model_Bookings::getInstance();
    	$bookingId = $bookingModel->cancelBooking($postData);
    	echo json_encode(array("status"=>"success","bookingId"=>$bookingId));
    	$this->_helper->viewRenderer->setNoRender();
    	$this->_helper->layout()->disableLayout();
    }
    
    public function getAllBookingsForDateAction(){
    	$date = $this->_request->getParam("date");
    	if(!empty($date)){
   			$bookingsModel = SOAPY_Model_Bookings::getInstance();
   			$maintanenceModel = SOAPY_Model_SlotMaintenance::getInstance();
   			echo json_encode(array("status"=>"success","bookings"=>$bookingsModel->getAllBookings($date),"maintanenceSlots"=>$maintanenceModel->getMaintenanceList($date)));
   			
    	}
    	else{
    		echo json_encode(array("status"=>"error", "message"=>"Date is not valid"));
    	}
    	$this->_helper->viewRenderer->setNoRender();
    	$this->_helper->layout()->disableLayout();
    }
    
    
    

}

