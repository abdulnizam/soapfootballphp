<?php


class Admin_UserController extends Zend_Controller_Action {
	
	
	
	public function init(){
	
		
	}
	
	public function addUserAction(){
		
	}
	
	public function changePasswordAction(){
		
	}
	public function doChangePasswordAction(){
		$rawBody = $this->_request->getRawBody();
		$postData = Zend_Json::decode($rawBody, Zend_Json::TYPE_ARRAY);
		
		$userIdentity = Zend_Auth::getInstance()->getIdentity();
		
		if(!empty($postData["currentPassword"]) && !empty($postData["newPassword"])){
			$result = $this->_userModel->changePassword($userIdentity->id, $postData["currentPassword"], $postData["newPassword"]);
			if(!empty($result)){
				echo json_encode(array("status"=>"success", "successMsg" => "Password Changed Successfully"));
			}
			else{
				echo json_encode(array("status"=>"error", "errorMsg" => "Current Password not matched"));
			}
		}
		else{
			echo json_encode(array("status"=>"error", "errorMsg" => "Invalid Data"));
		}
		
		$this->_helper->viewRenderer->setNoRender(true);
		$this->_helper->layout()->disableLayout();
	}
	
	public function editProfileAction(){
		
	}
	
	public function logoutAction(){
		Zend_Auth::getInstance()->clearIdentity();
		$this->_redirect("user/login/login");
	}
	public function profileAction(){
		
	}
	
	
	
	
}

