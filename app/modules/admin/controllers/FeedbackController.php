<?php

class Admin_FeedbackController extends Zend_Controller_Action
{

	
    public function init()
    {
        /* Initialize action controller here */
    	
    	$this->view->saveFeedbackAPI = $this->view->baseURL."/admin/feedback/save";
    }

    public function indexAction()
    {
        // action body
        $config = Zend_Registry::get("config");
        $questions = $config['feedback']['questions'];
        $this->view->questions = json_encode($questions);
        $this->_helper->layout()->disableLayout();
        
    }
    public function saveAction(){
    	try{
	    	$rawBody = $this->getRequest()->getRawBody();
	    	$postData = Zend_Json::decode($rawBody, Zend_Json::TYPE_ARRAY);
	    	$feedbackId = SOAPY_Model_Feedback::getInstance()->entry($postData);
	    	echo json_encode(array("status"=>"success"));
	   }
    	catch(Exception $ex){
    		echo json_encode(array("status"=>"error", "message"=>$ex->getMessage()));
    	}
    	$this->_helper->viewRenderer->setNoRender();
    	$this->_helper->layout()->disableLayout();
    }

}

