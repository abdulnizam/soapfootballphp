<?php


class User_LoginController extends Zend_Controller_Action {
	
	/**
	 * 
	 * @var STRGYM_Model_ResetLink
	 */
	protected $_resetLinkModel;
	
	/**
	 * 
	 * @var STRGYM_Model_Employees
	 */
	protected $_userModel;
	
	public function init(){
		$baseUrl = $this->getFrontController()->getBaseUrl();
		
		$this->_resetLinkModel = STRGYM_Model_ResetLink::getInstance();
		$this->_userModel = STRGYM_Model_Employees::getInstance();
		
		
		
	}
	
	public function loginAction(){
		
	}
	
	public function doLoginAction(){
		$rawBody = $this->_request->getRawBody();
		$postData = Zend_Json::decode($rawBody, Zend_Json::TYPE_ARRAY);
		$db = Zend_Registry::get("defaultDB");
		$authAdapter = new Zend_Auth_Adapter_DbTable($db);
		$authAdapter->setTableName("staar_users");
		$authAdapter->setIdentityColumn("employee_email");
		$authAdapter->setIdentity($postData["email"]);
		$authAdapter->setCredentialColumn("staar_password");
		$authAdapter->setCredential($postData["password"]);
		$authAdapter->setCredentialTreatment("MD5(?)");
		$result = $authAdapter->authenticate();
		if($result->isValid()){
			$rowObject = $authAdapter->getResultRowObject(array("id","employee_code","employee_name","employee_email","user_level"));
			/* if($rowObject->is_active == 1){ */
				$auth = Zend_Auth::getInstance();
				$auth->clearIdentity();
				$auth->getStorage()->write($rowObject);
				
				$redirectUrl = $this->view->members->listUrl;
				echo json_encode(array("status"=>"success", "redirectUrl"=>$redirectUrl));
			}
			else{
				echo json_encode(array("status"=>"error", "errorMsg"=>"Invalid username and password. Please try again."));
			}
		/* }
		else{
			echo json_encode(array("status"=>"error", "errorMsg"=>"Invalid username and password. Please try again."));
		} */
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		
	}
	
	public function sendResetLinkAction(){
		
		$config = Zend_Registry::get("config");
		
		
		$rawBody = $this->getRequest()->getRawBody();
		$postData = Zend_Json::decode($rawBody, Zend_Json::TYPE_ARRAY);
		$emailId = $postData['email'];
		if(!empty($emailId)){
			$userRecord = $this->_userModel->getUserByEmail($emailId);
			if(!empty($userRecord)){
				$resetLink = $this->_resetLinkModel->saveResetLink($emailId);
				if(!empty($resetLink)){
					$data = array();
					$data['reset_link'] = $this->view->user->resetPasswordUrl."/code/".$resetLink;
					
					STRGYM_Helper_MailHelper::sendEmployeeForgotPasswordMail($emailId, $data);
					
					echo json_encode(array("status"=>"success","successMsg"=>"Reset link is sent to your email id. please log into your email to reset the password."));
				}
			}
			else{
				echo json_encode(array("status"=>"error", "errorMsg"=>"Email is not registered with Staar Gym"));
			}
		}else{
			echo json_encode(array("status"=>"error", "errorMsg"=>"Invalid Email Id"));
		}
		
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
	}
	
	public function resetPasswordAction(){
		$resetId = $this->getRequest()->getParam("code");
		if(!empty($resetId)){
			$resetRecord = $this->_resetLinkModel->findByResetLinkId($resetId);
			if(!empty($resetRecord)){
				$this->view->invalidResetLink = false;
				$session = new Zend_Session_Namespace("RESET_LINK", true);
				$session->reset_email = $resetRecord["email_id"];
				$session->reset_link = $resetRecord["reset_link_id"];
			}
			else{
				$this->view->invalidResetLink = true;
			}
		}
		else{
			$this->view->invalidResetLink = true;
		}
	}
	
	public function doResetPasswordAction(){
		$config = Zend_Registry::get("config");
		
		
		$rawBody = $this->getRequest()->getRawBody();
		$postData = Zend_Json::decode($rawBody, Zend_Json::TYPE_ARRAY);
		$newPassword = $postData['newPassword'];
		$session = new Zend_Session_Namespace("RESET_LINK", true);
		$email = $session->reset_email;
		
		$this->_userModel->resetPassword($email, $newPassword);
		$this->_resetLinkModel->delete(array("reset_link_id = ?"=>$session->reset_link));
		
		echo json_encode(array("status"=>"success", "successMsg"=>"Password reset completed successfully. Please login now"));
		
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
	}
	
	public function forgotPasswordAction(){
		
	}
	
	
}

