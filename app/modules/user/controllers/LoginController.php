<?php


class User_LoginController extends Zend_Controller_Action {
	
	public function init(){
		$baseUrl = $this->getFrontController()->getBaseUrl();
		
		
		
	}
	
	public function loginAction(){
		
	}
	
	public function doLoginAction(){
		$rawBody = $this->_request->getRawBody();
		$postData = Zend_Json::decode($rawBody, Zend_Json::TYPE_ARRAY);
		$db = Zend_Registry::get("defaultDB");
		$authAdapter = new Zend_Auth_Adapter_DbTable($db);
		$authAdapter->setTableName("soapy_users");
		$authAdapter->setIdentityColumn("username");
		$authAdapter->setIdentity($postData["username"]);
		$authAdapter->setCredentialColumn("soapy_password");
		$authAdapter->setCredential($postData["password"]);
		$authAdapter->setCredentialTreatment("MD5(?)");
		$result = $authAdapter->authenticate();
		if($result->isValid()){
			$rowObject = $authAdapter->getResultRowObject(array("id","username","is_admin"));
			/* if($rowObject->is_active == 1){ */
				$auth = Zend_Auth::getInstance();
				$auth->clearIdentity();
				$auth->getStorage()->write($rowObject);
				
				$redirectUrl = $this->view->dashboardUrl;
				echo json_encode(array("status"=>"success", "redirectUrl"=>$redirectUrl));
			}
			else{
				echo json_encode(array("status"=>"error", "errorMsg"=>"Invalid username and password. Please try again."));
			}
		/* }
		else{
			echo json_encode(array("status"=>"error", "errorMsg"=>"Invalid username and password. Please try again."));
		} */
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		
	}
	
	
}

