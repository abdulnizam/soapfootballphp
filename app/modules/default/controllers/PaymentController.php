<?php

class PaymentController extends Zend_Controller_Action
{

	
    public function init()
    {
         
    }

    public function indexAction()
    {
        
    	
    }
    public function payumoneyRequestHandler(){
    	$config = Zend_Registry::get("config");
    	$env = $config['payment']['env'];
    	$orderId = $this->getRequest()->getParam("orderId");
    	$bookingsModel = SOAPY_Model_Bookings::getInstance();
    	$bookingsByOrderId = $bookingsModel->getAllBookingsByOrderId($orderId);
    	$noOfBookings = count($bookingsByOrderId);
    	$data['key'] = $config['payment']['payumoney']['merchant_key'];
    	$data['txnid'] = $orderId;
    	$data['amount'] = $noOfBookings * $config['advanceAmount'];
    	$data['firstname'] = $bookingsByOrderId[0]['first_name'] . " " . $bookingsByOrderId[0]['last_name'];
    	$data['email'] = $bookingsByOrderId[0]['email_id'];
    	$data['phone'] = $bookingsByOrderId[0]['mobile_no'];
    	$data['productinfo'] = "Soap Football Slot Booking Advance Amount";
    	$data['surl'] = $this->view->baseURL."/payment/payu-success";
    	$data['furl'] = $this->view->baseURL."/payment/payu-fail";
    	$data['service_provider'] = "payu_paisa";
    	$data['hash'] = SOAPY_Helper_Payment_PayUMoney::getRequestHash($data);
    	$this->view->gatewayUrl = SOAPY_Helper_Payment_PayUMoney::getGatewayUrl($env);
    	$this->view->data = $data;
    	
    	$this->renderScript("payment/payu-request-handler.phtml");
    	$this->_helper->layout()->disableLayout();
    	 
    	 
    	
    }
    public function ccavenueRequestHandler(){
    	$config = Zend_Registry::get("config");
    	$env = $config['payment']['env'];
    	
    	 
    	$orderId = $this->getRequest()->getParam("orderId");
    	$bookingsModel = SOAPY_Model_Bookings::getInstance();
    	$bookingsByOrderId = $bookingsModel->getAllBookingsByOrderId($orderId);
    	$noOfBookings = count($bookingsByOrderId);
    	$data['billing_name'] = $bookingsByOrderId[0]['first_name'] . " " . $bookingsByOrderId[0]['last_name'];
    	$data['billing_email'] = $bookingsByOrderId[0]['email_id'];
    	$data['billing_tel'] = $bookingsByOrderId[0]['mobile_no'];
    	$data['billing_country'] = "India";
    	if($env == "test"){
    		$data['billing_zip'] = "600034";
    		$data['billing_state'] = "Tamil Nadu";
    		$data['billing_city'] = "Chennai";
    		$data['billing_address'] = "NO 144/1, Nungambakkam High Road";
    	}
    	$data['tid'] = time() * 1000;
    	$data['merchant_id'] = $config['payment']['merchantId'];
    	$data['amount'] = $noOfBookings * $config['advanceAmount'];
    	$data['redirect_url'] = $this->view->baseURL."/payment/response-handler";
    	$data['cancel_url'] = $this->view->baseURL."/payment/response-handler";
    	$data['order_id'] = $orderId;
    	$data['currency'] = "INR";
  		$this->view->gatewayUrl = SOAPY_Helper_Payment_CCAvenue::getGatewayUrl($env);
    	$this->view->encryptedData = SOAPY_Helper_Payment_CCAvenue::getEncryptedData($data, $env);
    	$this->view->accessCode = SOAPY_Helper_Payment_CCAvenue::getAccessCode($env);
    	$this->renderScript('payment/ccavenue-request-handler.phtml');
    	$this->_helper->layout()->disableLayout();
    }
    public function requestHandlerAction(){
    	$config = Zend_Registry::get("config");
    	
    	if($config['payment']['skipPayments'] == 1){
    		$orderId = $this->getRequest()->getParam("orderId");
    		 
    		$bookingsModel = SOAPY_Model_Bookings::getInstance();
    		$paymentComments = "Payment Skipped for testing";
    		$bookingsModel->setOrderStatusSuccess($orderId, $paymentComments);
    		$this->_redirect($this->view->baseURL."/default/index/view-order/orderId/".$orderId);
    	}
    	else{
	    	if($config['paymentGateway'] == "ccavenue"){
	    		$this->ccavenueRequestHandler();
	    	}
	    	elseif($config['paymentGateway'] == "payumoney"){
	    		$this->payumoneyRequestHandler();
	    	}
    	}
    }
    
    public function responseHandlerAction(){
    	$config = Zend_Registry::get("config");
    	$env = $config['payment']['env'];
    	$encResponseData = $this->getRequest()->getParam("encResp");
    	$orderStatus = SOAPY_Helper_Payment_CCAvenue::getOrderStatusWithInformation($encResponseData, $env);
    	$orderInfo = $orderStatus['orderInfo'];
    	$orderId = $orderInfo["order_id"];
    	$paymentData = array();
    	$paymentData['order_id'] = $orderId;
    	$paymentData['tracking_id'] = $orderInfo['tracking_id'];
    	$paymentData['payment_status'] = $orderInfo['order_status'];
    	$paymentData['bank_ref_no'] = $orderInfo['bank_ref_no'];
    	$paymentData['payment_info'] = $orderInfo;
    	$paymentTrackingId = SOAPY_Model_Payments::getInstance()->entry($paymentData);
    	if(strtolower($orderStatus['status']) == "success"){
    		$bookingsModel = SOAPY_Model_Bookings::getInstance();
    		$bookingDetails = $bookingsModel->getAllBookingsByOrderId($orderId);
    		$orderDetails = $bookingDetails[0];
    		$paymentComments = "Payment Tracking ID : ". $paymentTrackingId." <br/> CCAVENUE Tracking ID : ". $paymentData['tracking_id']." <br/> Bank Ref No : ". $paymentData['bank_ref_no'];
    		$bookingsModel->setOrderStatusSuccess($orderId, $paymentComments);
    		
     		$mailData = array();
     		$mailData['name'] = $orderDetails['first_name'] . " ". $orderDetails['last_name'];
     		$mailData['order_id'] = $orderDetails['order_id'];
    		$mailData['status'] = "Booking Confirmed";
     		$mailData['mobile'] = $orderDetails['mobile_no'];
     		$mailData['slots'] = array();
     		foreach($bookingDetails as $bookingDetails){
     			array_push($mailData['slots'], array('booking_date'=>$bookingDetails['booking_date'], 'slot'=>$bookingDetails['slot']));
     		}
     		SOAPY_Helper_MailHelper::sendConfirmationMail($orderDetails['email_id'], $mailData);
    	}
    	else{
    		$bookingsModel = SOAPY_Model_Bookings::getInstance();
    		$paymentComments = "Payment Tracking ID : ". $paymentTrackingId." <br/> CCAVENUE Tracking ID : ". $paymentData['tracking_id']." <br/> Failiure Reason : ". $orderInfo['failure_message'];
    		$bookingsModel->setOrderStatusPaymentFailiure($orderId, $paymentComments);
    		
    	}
    	$this->_redirect($this->view->baseURL."/default/index/view-order/orderId/".$orderId);
    }

public function testEmailAction() {
$mailData = array();
                $mailData['name'] = "Test Name";
                $mailData['order_id'] = "Test Order";
                $mailData['status'] = "Test Booking Confirmed";
                $mailData['mobile'] = "9710529454";
                $mailData['slots'] = array();
            SOAPY_Helper_MailHelper::sendConfirmationMail("abdulnizam89@gmail.com", $mailData);
echo "success";
$this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();
}

    public function responseEventAction()
    {
        $config = Zend_Registry::get("config");
        $env = $config['payment']['env'];
        $eventModel = SOAPY_Model_EventLog::getInstance();

        // $rawBody = $this->_request->getRawBody();
        // $orderInfo = Zend_Json::decode($rawBody, Zend_Json::TYPE_ARRAY);  
        // $orderStatus['status'] = 'success';
       
        $encResponseData = $this->getRequest()->getParam("encResp");
        $orderStatus = SOAPY_Helper_Payment_CCAvenue::getOrderStatusWithInformation($encResponseData, $env);
        $orderInfo = $orderStatus['orderInfo'];
        
        $orderId = $orderInfo["order_id"]; 
        $paymentComments = $orderInfo['status_message'];
        $bookingsModel = SOAPY_Model_Bookings::getInstance();

        // log
        $data['reference'] = $orderId;
        $data['payment_load'] = $orderStatus;
        $data['message'] = $paymentComments;
        $data['status'] = $orderStatus['status'];
        // log end
        if( strtolower($orderStatus['status']) == "success"){
		
		$bookingsModel->setOrderStatusSuccess($orderId, $paymentComments);
           	$bookingDetails = $bookingsModel->getAllBookingsByOrderId($orderId);
		$orderDetails = $bookingDetails[0];
		$mailData = array();
		$mailData['name'] = $orderDetails['first_name'] . " ". $orderDetails['last_name'];
		$mailData['order_id'] = $orderDetails['order_id'];
		$mailData['status'] = "Booking Confirmed";
		$mailData['mobile'] = $orderDetails['mobile_no'];
		$mailData['slots'] = array();
		foreach($bookingDetails as $bookingDetails){
			array_push($mailData['slots'], array('booking_date'=>$bookingDetails['booking_date'], 'slot'=>$bookingDetails['slot']));
		}
		SOAPY_Helper_MailHelper::sendConfirmationMail($orderDetails['email_id'], $mailData);
            
        }
        else{
            // $paymentComments = "status";
            $bookingsModel->setOrderStatusPaymentFailiure($orderId, $paymentComments);
            
        }

        $eventModel->eventLog( $data );

        echo json_encode(array("status"=>"success","orderId"=>$orderId, "responseStatus" => $paymentComments));

        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();
    }

	public function payuSuccessAction(){
    	$config = Zend_Registry::get("config");
    	$env = $config['payment']['env'];
    	$data = $this->getRequest()->getParams();
    	$encryptedHash = SOAPY_Helper_Payment_PayUMoney::getResponseHash($data);
    	$bookingsModel = SOAPY_Model_Bookings::getInstance();
    	$orderId = $data['txnid'];
    	
    	$paymentData = array();
    	$paymentData['order_id'] = $orderId;
    	$paymentData['tracking_id'] = $data['mihpayid'];
    	$paymentData['payment_status'] = $data['failure'];
    	$paymentData['bank_ref_no'] = $data['bank_ref_num'];
    	$paymentData['payment_info'] = $data;
    	$paymentTrackingId = SOAPY_Model_Payments::getInstance()->entry($paymentData);
    	if($data['hash'] != $encryptedHash){
    		$bookingsModel->setOrderStatusPaymentFailiure($orderId, "Invalid Transaction");
    	}
    	else{
    		$paymentComments = "Payment Tracking ID : ". $paymentTrackingId." <br/> CCAVENUE Tracking ID : ". $paymentData['tracking_id']." <br/> Bank Ref No : ". $paymentData['bank_ref_no'];
    		$bookingsModel->setOrderStatusSuccess($orderId, $paymentComments);
    		
    		$bookingDetails = $bookingsModel->getAllBookingsByOrderId($orderId);
    		$orderDetails = $bookingDetails[0];
    		
    		$mailData = array();
    		$mailData['name'] = $orderDetails['first_name'] . " ". $orderDetails['last_name'];
    		$mailData['order_id'] = $orderDetails['order_id'];
    		$mailData['status'] = "Booking Confirmed";
    		$mailData['mobile'] = $orderDetails['mobile_no'];
    		$mailData['slots'] = array();
    		foreach($bookingDetails as $bookingDetails){
    			array_push($mailData['slots'], array('booking_date'=>$bookingDetails['booking_date'], 'slot'=>$bookingDetails['slot']));
    		}
    		SOAPY_Helper_MailHelper::sendConfirmationMail($orderDetails['email_id'], $mailData);
    		 
    	}
    	
    	$this->_redirect($this->view->baseURL."/default/index/view-order/orderId/".$orderId);
    	 
    }
    public function payuFailAction(){
    	$config = Zend_Registry::get("config");
    	$env = $config['payment']['env'];
    	$data = $this->getRequest()->getParams();
    	$encryptedHash = SOAPY_Helper_Payment_PayUMoney::getResponseHash($data);
    	$bookingsModel = SOAPY_Model_Bookings::getInstance();
    	$orderId = $data['txnid'];
    	
    	$paymentData = array();
    	$paymentData['order_id'] = $orderId;
    	$paymentData['tracking_id'] = $data['mihpayid'];
    	$paymentData['payment_status'] = $data['failure'];
    	$paymentData['bank_ref_no'] = $data['bank_ref_num'];
    	$paymentData['payment_info'] = $data;
    	$paymentTrackingId = SOAPY_Model_Payments::getInstance()->entry($paymentData);
    	 
    	if($data['hash'] != $encryptedHash){
    		$bookingsModel->setOrderStatusPaymentFailiure($orderId, "Invalid Transaction");
    	}
    	else{
    		$bookingsModel->setOrderStatusPaymentFailiure($orderId, "Cancelled By User");
    	}
    	$this->_redirect($this->view->baseURL."/default/index/view-order/orderId/".$orderId);
    }
    


}



