<?php

class IndexController extends Zend_Controller_Action
{

	
    public function init()
    {
        /* Initialize action controller here */
    	$this->view->paymentRequestHandler = $this->view->baseURL."/payment/request-handler";
    }

    public function indexAction()
    {
        // action body
        
    	$this->view->totalCourtCount = 2;
    	
    }
    
    public function viewOrderAction(){
    	$orderId = $this->_request->getParam("orderId");
    	if(!empty($orderId)){
    		$bookingsModel = SOAPY_Model_Bookings::getInstance();
    		$bookingList = $bookingsModel->getAllBookingsByOrderId($orderId);
    		$orderDetails = array("slots"=>array());
    		foreach($bookingList as $booking){
    			if(empty($oderDetails)){
    				$orderDetails['orderId'] = $orderId;
    				$orderDetails['name'] = $booking['first_name']." ".$booking['last_name'];
    				$orderDetails['email'] = $booking['email_id'];
    				$orderDetails['phone'] = $booking['mobile_no'];
    				$orderDetails['status'] = $booking["status"];
    			}
    			$slotInfoArr = array("date"=>$booking['booking_date'], "slot"=>$booking['slot']);
    			$orderDetails['slots'][] = $slotInfoArr;
    		}
    		$this->view->orderDetails = $orderDetails;
    		
    	}
    	else{
    		$this->view->errorMessage = "No Order Id Found";
    	}
    	
    	$this->_helper->layout()->disableLayout();
    	
    }
    public function getAvailableSlotsAction(){
    	$date = $this->_request->getParam('date');
    	$slotMasterModel = SOAPY_Model_SlotMaster::getInstance();
    	$slots = $slotMasterModel->getAllAvailableSlots($date);
    	$publicHoliday = $slotMasterModel->isPublicHoliday($date);
    	echo json_encode(array("status"=>"success", "slots"=>$slots));
    	
    	$this->_helper->viewRenderer->setNoRender();
    	$this->_helper->layout()->disableLayout();
    }
    public function bookAction(){
    	$rawBody = $this->_request->getRawBody();
    	$postData = Zend_Json::decode($rawBody, Zend_Json::TYPE_ARRAY);
    	$bookingsModel = SOAPY_Model_Bookings::getInstance();
    	$orderId = $bookingsModel->save($postData['bookingDetails'], $postData['cartDetails']);
    	echo json_encode(array("status"=>"success","orderId"=>$orderId));
    	$this->_helper->viewRenderer->setNoRender();
    	$this->_helper->layout()->disableLayout();
    }

    public function termsconditionsAction() {
		
    }


}

